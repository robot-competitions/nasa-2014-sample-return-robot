#ifndef PololuSimple_Include
#define PololuSimple_Include
//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//
#include "PololuBase.h"
//---------------------------------------------------------------------------------------------------------------------
class PololuSimple : public PololuBase
{
public:

    PololuSimple(const unsigned char  device, SerialPort& sp);
    ~PololuSimple();

    void brake(const unsigned char  speed) const /* 0 to 32 */;

    size_t getErrorStatus() const;
    size_t getErrorOccurred();
    size_t getLimitStatus() const;

    size_t getAnalog1Raw() const;
    size_t getAnalog1UnlimitedRaw() const;
    int getAnalog1Scaled() const;
    size_t getAnalog2Raw() const;
    size_t getAnalog2UnlimitedRaw() const;
    int getAnalog2Scaled() const;

    size_t getRC1Raw() const;
    size_t getRC1UnlimitedRaw() const;
    int getRC1Scaled() const;
    size_t getRC2Raw() const;
    size_t getRC2UnlimitedRaw() const;
    int getRC2Scaled() const;

    inline size_t getTemp() const;
    inline size_t getVin() const;

    inline size_t getForward() const;
    inline size_t getTarget() const;

    // error conditions functions
    inline bool isSafeErr() const;
    inline bool isKillSwitch() const;
    inline bool isLowVin() const;
    inline bool isOverTemp() const;


    // control functions
    void exitSafeStart() const;

    void forward(const int speed) const;
    void forwardShort(const unsigned char  speed) const;
    void forwardPercent(const unsigned char  percent_speed) const;

    void reverse(const int speed) const;
    void reverseShort(const unsigned char  speed) const;
    void reversePercent(const unsigned char  percent_speed) const;

    void stop() const;

    void version(unsigned char  version[4]) const;

private:

    enum ERRS { 
        SAFE_START= 0x01, CHANNEL_INVALID = 0x02, SERIAL_ERR = 0x04, CMD_TIMEOUT = 0X08,
        KILL_SWITCH = 0x10, LOW_VIN = 0x20, HIGH_VIN = 0x40, OVER_TEMP = 0x80,
        DRIVER_ERR = 0x100, ERR_HIGH = 0x200
    };

    enum {
        SAFE_START_CODE = 0x03,
        FORWARD_CODE = 0x05, REVERSE_CODE = 0x06, 
        FORWARD_SHORT_CODE = 0x09, REVERSE_SHORT_CODE = 0x0A,
        
        GET_VAR_CODE = 0x21,
        GET_VER_CODE = 0x42,
        
        // Variable registers
        ERROR_REG = 0, ERROR_OCCURED_REG, SERIAL_ERR_REG, LIMIT_REG, 
        RC1_UNLIMITED = 4, RC1_RAW, RC1_SCALED, 
        RC2_UNLIMITED = 8, RC2_RAW, RC2_SCALED,
      
        ANALOG1_UNLIMITED = 12, ANALOG1_RAW, ANALOG1_SCALED,
        ANALOG2_UNLIMITED = 16, ANALOG2_RAW, ANALOG2_SCALED,

        TARGET_REG = 20, SPEED_REG, BRAKE_REG, INPUT_V_REG, TEMP_REG, RC1_PERIOD, BAUD_REG, SYS_TIME_LOW, SYS_TIME_HI,
        RESET_REG = 127,
    };

    int getVariable(unsigned char  variable_id) const;
    bool mIsKillSw;
    bool mIsOverTemp;
    bool mIsLowVin;
    bool mIsSafeStartErr;
};
//----------------------------------------------------------------------------------------------------------------------
inline size_t PololuSimple::getErrorStatus() const { return getVariable(ERROR_REG); }
inline size_t PololuSimple::getLimitStatus() const { return getVariable(LIMIT_REG); }
inline size_t PololuSimple::getTemp() const { return getVariable(TEMP_REG); }
inline size_t PololuSimple::getVin() const { return getVariable(INPUT_V_REG); }
inline size_t PololuSimple::getTarget() const { return getVariable(TARGET_REG); }
inline size_t PololuSimple::getForward() const { return getVariable(SPEED_REG); }
//----------------------------------------------------------------------------------------------------------------------
inline size_t PololuSimple::getAnalog1Raw() const { return getVariable(ANALOG1_RAW); }
inline size_t PololuSimple::getAnalog1UnlimitedRaw() const { return getVariable(ANALOG1_UNLIMITED); }
inline int PololuSimple::getAnalog1Scaled() const { return getVariable(ANALOG1_SCALED); }

inline size_t PololuSimple::getAnalog2Raw() const { return getVariable(ANALOG2_RAW); }
inline size_t PololuSimple::getAnalog2UnlimitedRaw() const { return getVariable(ANALOG2_UNLIMITED); }
inline int PololuSimple::getAnalog2Scaled()  const{ return getVariable(ANALOG2_SCALED); }
//----------------------------------------------------------------------------------------------------------------------
inline size_t PololuSimple::getRC1Raw() const { return getVariable(RC1_RAW); }
inline size_t PololuSimple::getRC1UnlimitedRaw() const { return getVariable(RC1_UNLIMITED); }
inline int PololuSimple::getRC1Scaled() const { return getVariable(RC1_SCALED); }

inline size_t PololuSimple::getRC2Raw() const { return getVariable(RC2_RAW); }
inline size_t PololuSimple::getRC2UnlimitedRaw() const { return getVariable(RC2_UNLIMITED); }
inline int PololuSimple::getRC2Scaled() const { return getVariable(RC2_SCALED); }
//----------------------------------------------------------------------------------------------------------------------
// call these only after reading error and limit status
inline bool PololuSimple::isSafeErr() const { return mIsSafeStartErr; };
inline bool PololuSimple::isKillSwitch() const { return mIsKillSw; };
inline bool PololuSimple::isOverTemp() const { return mIsOverTemp; };
inline bool PololuSimple::isLowVin() const { return mIsLowVin; };

#endif //PololuSimple _Include

