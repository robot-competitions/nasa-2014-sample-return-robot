#ifndef Maestro_Include
#define Maestro_Include
//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//
// This class uses the Command port - not the TTL port
// The Maestro should be set to either USB Dual Port or Chained.
// CRC is enabled
// 
#include <utility>
using namespace std;
class SerialPort;
#include "PololuBase.h"
//---------------------------------------------------------------------------------------------------------------------
class Maestro : public PololuBase
{
public:
    typedef pair<size_t, size_t> Position;

    // Input values
    static const unsigned char  MIN_ANALOG_CHANNEL = 0;   // can be used as digital input 
    static const unsigned char  MAX_ANALOG_CHANNEL = 11;

    static const size_t MAX_ANALOG_VOLTAGE = 5; // volts
    static const size_t MAX_ANALOG_VALUE = 1023;

    static const unsigned char  MIN_DIGITAL_CHANNEL = 12; // first non-analog input channel
    static const unsigned char  MAX_DIGITAL_CHANNEL = 23;

    static const size_t MID_DIGITAL_VALUE = 1023 / 2;   // < input is low, > input is on

    // Digital output values
    static const size_t MID_DIGITAL_OUT = 6000; // < input is low, > input is on

    // Members functions -----------------------------------------------------------------------------------------------
    Maestro(const unsigned char  mDevice, SerialPort& sp) : PololuBase(mDevice, sp) {}

    size_t getMilliseconds(const unsigned char  channel = 0) const;
    void setMilliseconds(const size_t value, const unsigned char  channel = 0) const;

    size_t getQuarterMilliseconds(const unsigned char  channel = 0) const;
    void setQuarterMilliseconds(const size_t value, const unsigned char  channel = 0) const; // Set Target

    // time is quarter milliseconds
    void setMultipleTargets(const unsigned char  count, const size_t targets[], const unsigned char  first_channel = 0);

    float getAnalogIn(const unsigned char  channel = 0) const;
    bool getDigitalInput(const unsigned char  channel = 0) const;

    bool getDigitalOut(const unsigned char  channel = 0) const;
    void setDigitalOut(const bool value, const unsigned char  channel = 0) const;

    bool isMoving() const;
    size_t getErrors() const;

    void goHome() const;
    void setPWM(const size_t high_time, const size_t period) const;

    void setAcceleration(const size_t accel, const unsigned char  channel = 0) const;

    void setSpeed(const size_t speed, const unsigned char  channel = 0) const;

private:
};
//---------------------------------------------------------------------------------------------------------------------
inline
bool operator==(const Maestro::Position& lhs, const Maestro::Position& rhs) 
{ 
    return (lhs.first == rhs.first) && (lhs.second == rhs.second); 
}
//---------------------------------------------------------------------------------------------------------------------
inline
bool operator!=(const Maestro::Position& lhs, const Maestro::Position& rhs) 
{
    return ! operator==(lhs, rhs);
}
//---------------------------------------------------------------------------------------------------------------------
inline
size_t Maestro::getMilliseconds(const unsigned char  channel /*= 0*/) const
{
    return getQuarterMilliseconds(channel) / 4;
}
//---------------------------------------------------------------------------------------------------------------------
inline
void Maestro::setMilliseconds(const size_t value, const unsigned char  channel /*= 0*/) const
{
    size_t v = static_cast <size_t>(value * 4);
    setQuarterMilliseconds(v, channel);
}
//---------------------------------------------------------------------------------------------------------------------
inline
bool Maestro::getDigitalInput(const unsigned char  channel /*= 0*/) const
{
    return getQuarterMilliseconds(channel) > MID_DIGITAL_VALUE;
}
//---------------------------------------------------------------------------------------------------------------------
inline
bool Maestro::getDigitalOut(const unsigned char  channel) const
{
    return getQuarterMilliseconds(channel) > MID_DIGITAL_OUT;
}
//---------------------------------------------------------------------------------------------------------------------
inline
void Maestro::setDigitalOut(const bool value, const unsigned char  channel /*= 0*/) const
{
    setQuarterMilliseconds(value ? 12000 : 0, channel);
}

#endif // Maestro_Include

