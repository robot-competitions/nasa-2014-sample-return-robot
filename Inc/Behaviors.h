#ifndef Behaviors_Include
#define Behaviors_Include
//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <ctime>
using namespace std;

#include "BehaviorAbstract.h"
#include "Goal.h"
#include "RobotControl.h"
#include "RobotData.h"
#include "Timer.h"
#include "Move.h"
#include "FindImage.h"
#include "ServoMove.h"

#include "PIDControl.h"
//=====================================================================================================================
class Cruise : public BehaviorAbstract
{
public:
    Cruise()  : BehaviorAbstract("Cruise") {}
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);
};
//=====================================================================================================================
//  Executes a second behavior but inverts the preemption results
//  
class Invert : public BehaviorAbstract, protected IndirectMixIn
{
public:
    Invert(Behavior b)  : BehaviorAbstract("Invert"), IndirectMixIn(b) {}
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& sub) { 
        exec(rd, rc, sub); 
        mPreempted = !mPreempted;
    }
};
//=====================================================================================================================
class LED : public BehaviorAbstract,  Timer
{
public:
    LED() : BehaviorAbstract("LED"), Timer(500), mLedState(false) {}
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

private:
    bool mLedState;
};
//======================================================================================================================
//  The Buttons class accepts in the constructor another behavior that is executed when the button, defined by 'key' 
//  template argument, is pressed
template <int key>
class Buttons : public BehaviorAbstract
{
public:
    Buttons(BehaviorAbstract& tv)  : BehaviorAbstract("Buttons"), mBehavior(tv) {}
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

private:
    BehaviorAbstract& mBehavior;
};
//======================================================================================================================
//  These two button classes accept in the constructor another behavior that is executed when the button is pressed
class CButton : public BehaviorAbstract
{
public:
    CButton(BehaviorAbstract& tv);
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

private:
    BehaviorAbstract& mBehavior;
};
//======================================================================================================================
class DButton : public BehaviorAbstract
{
public:
    DButton(BehaviorAbstract& tv);
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

private:
    BehaviorAbstract& mBehavior;
};
//======================================================================================================================
class NewGoal : public BehaviorAbstract
{
public:
    NewGoal(Goal& tv);
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

private:
    Goal& mGoal;
};
//======================================================================================================================
class OrangeFence : public BehaviorAbstract
{
public:
    OrangeFence() :  BehaviorAbstract("Orange Fence") { }
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);
};
//======================================================================================================================
class Panic : public BehaviorAbstract
{
public:
    Panic() :  BehaviorAbstract("Panic"), mLowVinCnt(0), mWasActive(false) { }
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

private:
    unsigned char  mLowVinCnt;
    bool mWasActive;
};
//======================================================================================================================
class RCControl : public BehaviorAbstract
{
public:
    RCControl() : BehaviorAbstract("RC Control") { }
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);
private:
    Behavior mBehavior;
};
//======================================================================================================================
class Reset : public BehaviorAbstract
{
public:
    Reset() :  BehaviorAbstract("Reset") { }
    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) { 
        if (!mPreempted) {
            resetAll();
            mPreempted = true;
        }
    }
};
//======================================================================================================================
class Track : public BehaviorAbstract
{
public:
    static const int timeout_limit = 10;
    Track() :  BehaviorAbstract("Track"), mPID(0.1, 0.1, 0.0, -100, 100) { }

    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

private:
    PIDControl mPID;
    int mLastX;
};
//======================================================================================================================
class TrackFiducial : public BehaviorAbstract
{
public:
    static const int timeout_limit = 10;
    TrackFiducial(const char* name) :  BehaviorAbstract("TrackFiducial"), mPID(0.1, 0.1, 0.0, -100, 100), mName(name) { }

    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

private:
    PIDControl mPID;
    string mName;
    int mLastX;
};

#endif // Behaviors_Include
