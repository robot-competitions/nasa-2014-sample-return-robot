#ifndef BehaviorAbstract_Include
#define BehaviorAbstract_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <vector>
#include "Configuration.h"

struct RobotData;
struct RobotControl;
class Subsumption;
//----------------------------------------------------------------------------------------------------------------------
class BehaviorAbstract;
typedef BehaviorAbstract* Behavior;
typedef Behavior TaskList[];
typedef std::vector<Behavior> TaskVector;
//======================================================================================================================
class BehaviorAbstract
{
public:
	BehaviorAbstract(const c_char* n) : mName(n) {}
	BehaviorAbstract() {}
	virtual void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) = 0;

    static void clearPreempted() { mPreempted = false; }
    bool preempt() const { return mPreempted; }

    virtual void reset() {}     // called when a goal is started to set all the values in the behaviors
    const c_char* who() { return mName; }    

    static void resetAll();
    static void setTasks(TaskVector& tasks) { mCurTasks = &tasks; }
    
protected:
    static void exec_reset_behaviors(Behavior b);

    const c_char* mName;    
    static TaskVector* mCurTasks;

    //----------------------------------------------------------------------------------------------------------------------
    // data used and shared among the behaviors - the behavior environment
    // 
    //  overall location of rover
    enum eLocation { start, platform, field };

    // overall orientation of rover
    enum eOrientation { N, NE, E, SE, S, SW, W, NW, unknown };

    // what image, if any, can rover currently see
    enum eSeeing { home_front, home_back, white_bldg, sample, obstacle, nothing };

    // status of rover, i.e. what doing
    enum eStatus { starting, carrying, searching, localizing };
    
    // where is image with respect to rover orientation
    //  180 degrees is directly ahead - this avoids positive / negative values
    //      left is > 180, right < 180
    //  front = within 30 degrees, otherwise left or right
    enum eWhere { front, left, right};

    static eLocation mLocation;
    static eOrientation mOrientation;
    static eStatus mStatus;
    static eWhere mWhere;

    static bool mPreempted;

    // image information - angle is in degrees based on pixel and RC milliseconds when seen
    static eSeeing mSeeing;
    static int mAngle;

    static int mRcPos;  // rc position in msecs
    static int mPosX;   // x position of image on camera in px
    //----------------------------------------------------------------------------------------------------------------------
    //
    //  Utility values and routines for converting pixels, RC msecs, and degrees
    static const size_t camera_field_of_view = 75; // degrees
    static const size_t image_width = 424; // pixels

    static const size_t rc_range = 1200; // msecs, roughly full left to full right
    static const size_t rc_middle = 1500;  // msecs, roughly straight forward msecs reading (sweep is 900 to 2100, roughly)

    static const int px_to_deg(const int px) { return (px * camera_field_of_view) / image_width; }
    static const int msec_to_deg(const int msec);
    static const void image_to_angle(const int px, const int msecs);

    static const void which_direction();
};
//======================================================================================================================
//  Mixin class to add a boolean state indicating it is active. 
class ActiveMixIn
{
public:
    ActiveMixIn() : mIsActive(false) { }

    bool isActive() const { return mIsActive; }
    void setActive() { mIsActive = true; }
    void resetActive() { mIsActive = false; }

private:
	bool mIsActive;
};
//======================================================================================================================
//  Mixin class to handle a behavior to be called by the main behavior
class IndirectMixIn
{
public:
    IndirectMixIn(Behavior b = 0) : mBehavior(b) {}

    void exec(RobotData& rd, RobotControl& rc, Subsumption& sub) { if (mBehavior) (*mBehavior)(rd, rc, sub); }
    void reset() { if (mBehavior) mBehavior->reset(); }

protected:
    Behavior mBehavior;
};

#endif // BehaviorAbstract_Include
