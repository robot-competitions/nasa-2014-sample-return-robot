#ifndef ElapsedTimer_Include
#define ElapsedTimer_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <ctime>
//======================================================================================================================
// The Timer class provides an elapsed time or delay measurement.
// 
class Timer
{
public:
    static const int NO_WAIT = 0;
    Timer(const int wait = NO_WAIT) : mPeriod(wait), mExpiration(clock_t(mPeriod)+clock()) {}

    bool isExpired() { return mExpiration < clock(); }
    bool perdiodCheck() { return !isExpired(); }

    void periodChange(const clock_t wait) { mPeriod = wait; }
    void timerStart() { mExpiration = clock_t(mPeriod) + clock(); }

protected:
    clock_t mPeriod;
    clock_t mExpiration;
};
#endif // ElapsedTimer_Include