#ifndef PIDControl_Include
#define PIDControl_Include
//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//=============================================================================
class PIDControl
{
public:
    PIDControl(const double p_gain, 
        const double i_gain,
        const double d_gain, 
        const double min_i,
        const double max_i);

    void clearIntegrator();
    double update(const double error);

    double proportionalGain() const { return mProportionalGain; }
    double derivativeGain() const { return mDerivativeGain; }
    double integralGain() const { return mIntegralGain; }
    double maxIntegrator() const { return mMaxIntegrator; }
    double minIntegrator() const { return mMinIntegrator; }
    double integrator() const { return mErrorSum; }

protected:

private:
    double mErrorSum;         // Integrator state
    double mMaxIntegrator; 
    double mMinIntegrator;      // Maximum and minimum allowable Integrator state

    double mIntegralGain;       // Integral gain
    double mProportionalGain;   // proportional gain
    double mDerivativeGain;     // derivative gain

    double mLastError;
};
//=============================================================================
inline 
PIDControl::PIDControl(const double p_gain, 
                       const double i_gain, 
                       const double d_gain,
                       const double min_i,
                       const double max_i) :
    mErrorSum(0),
    mMaxIntegrator(max_i),
    mMinIntegrator(min_i),
    mIntegralGain(i_gain),
    mProportionalGain(p_gain),
    mDerivativeGain(d_gain)
{
}

#endif // PIDControl_Include
