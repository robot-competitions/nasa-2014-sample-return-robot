#ifndef ThreadTimer_Include
#define ThreadTimer_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//#include <Windows.h>
//=====================================================================================================================
class ThreadTimer
{
public:
    ThreadTimer(const size_t milliseconds) : mWaitTime(milliseconds) {
//        LARGE_INTEGER liDueTime;
//        liDueTime.QuadPart=-0000000;
//
//        hTimer = CreateWaitableTimer(NULL, FALSE, 0);
//        SetWaitableTimer(hTimer, &liDueTime, mWaitTime, NULL, NULL, 0);
    }
    void wait() {
//    	WaitForSingleObject(hTimer, mWaitTime);
    }

    ~ThreadTimer() {}

private:
//    HANDLE hTimer;
    size_t mWaitTime;
};

#endif // ThreadTimer_Include
