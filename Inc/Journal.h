#ifndef Journal_Include
#define Journal_Include
//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//

#include <iosfwd>
#include <string>
#include <cstdio>
using namespace std;
#include "OS.h"
//=================================================================================================================
class JournalEmit {
public:
    enum Level {
        eDetail = 1, eTrace, eLog, eFunc = 20
    };

    JournalEmit(Level level, char const* fname, char const* version, bool usecerr = false);
    ~JournalEmit();

    static void setLevel(JournalEmit::Level val);

    static void cleanup();
    static void emit(Level level, char const* location, char const* text);
    static void emit(Level level, char const* location, char const* msg, char const* text);
    static void emit(Level level, char const* location, char const* msg, long const& value);

protected:
    static void newFile();
    static void emit(char const* location, char const* msg, char const* text, Level level);
    static void emitTime();
    static bool mActive;
//    static CRITICAL_SECTION mCriticalSection;
    static string mCurDate;
    static string mFNameBase;
    static string mFName;
    static Level mLevel;
    static ofstream* mLogFile;
    static ostream* mOutStream;
    static bool mUseCerr;
    static string mVersion;
};
//=================================================================================================================
class Journal {
public:
    Journal(JournalEmit::Level, char const* location);

    void operator()(char const* text) const {
        emit(text);
    }
    void operator()(string const& text) const {
        emit(text.c_str());
    }
    void operator()(string& text) const {
        emit(text.c_str());
    }

    void operator()(char const* text, bool const a, bool const b, bool const c, bool const d) const;

    void operator()(char const* msg, char const* text) const {
        emit(msg, text);
    }
    void operator()(char const* msg, long const& value) const {
        emit(msg, value);
    }
    void operator()(char const* msg, string const& text) const {
        emit(msg, text.c_str());
    }
    void operator()(char const* msg, string& text) const {
        emit(msg, text.c_str());
    }

    void operator()(char const* msg, char* s1, long const& v1, char* s2, long const& v2) const;
    void operator()(char const* msg, char* s1, long const& v1, char* s2, long const& v2, char* s3, long const& v3) const;

    void operator()(string const& msg, string const& text) const {
        emit(msg.c_str(), text.c_str());
    }
    void operator()(string const& msg, long const& value) const {
        emit(msg.c_str(), value);
    }

protected:
    void emit(char const* text) const {
        JournalEmit::emit(mLevel, mLocation.c_str(), text);
    }
    ;
    void emit(char const* msg, char const* text) const {
        JournalEmit::emit(mLevel, mLocation.c_str(), msg, text);
    }
    ;
    void emit(char const* msg, long const& value) const {
        JournalEmit::emit(mLevel, mLocation.c_str(), msg, value);
    }
    ;

private:
    Journal();
    Journal(Journal&);

    string mLocation;
    JournalEmit::Level mLevel;

};
//-----------------------------------------------------------------------------------------------------------------
inline
void JournalEmit::emit(Level level, char const* location, char const* text) {
    emit(level, location, "", text);
}
//-----------------------------------------------------------------------------------------------------------------
inline
void JournalEmit::emit(Level level, char const* location, char const* msg, char const* text) {
    if ((mOutStream) && (level >= mLevel)) {
        emit(location, msg, text, level);
    }
}
//-----------------------------------------------------------------------------------------------------------------
inline
void JournalEmit::emit(Level level, char const* location, char const* msg, long const& value) {
    char buf[15];
    if ((mOutStream) && (level >= mLevel)) {
        sprintf(buf, "%d", value);
        emit(location, msg, buf, level);
    }
}

#endif
