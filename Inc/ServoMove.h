#ifndef Servo_Include
#define Servo_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//----------------------------------------------------------------------------------------------------------------------
// Servo Control Values
// 
// back limits are limited for each rover by the Maestro settings
const size_t PAN_BACK_RIGHT = 900;
const size_t PAN_FORWARD = 1500;
const size_t PAN_BACK_LEFT = 2100;
const size_t PAN_RANGE = PAN_BACK_LEFT - PAN_BACK_RIGHT;

// tilt back limit is limited for each rover by the Maestro settings
const size_t TILT_BACK = 1800;
const size_t TILT_HORIZ = 1500;
const size_t TILT_SEARCH = 1300;
const size_t TILT_DOWN = 1150;

// up and down are limited for each rover by the Maestro settings to avoid burning out the servos. 
const size_t LIFT_UP = 2600;
const size_t LIFT_CARRY = 1600;
const size_t LIFT_DOWN = 850;

//======================================================================================================================
//  class to control movement of the robot servos. The base class constructor with default parameters provides positioning 
//  at a neutral position. Specific positions can be specified for other situations and provide a descriptive 
//  name for logging the behavior.
//  
//  Subclasses can add more complexity by overriding operator().
//  
class ServoMove : public BehaviorAbstract, public ActiveMixIn
{
public:
    explicit 
    ServoMove(const c_char* n = "Neutral", 
        const int pan = PAN_FORWARD, const int tilt = TILT_HORIZ, const int lift = LIFT_UP, 
        Behavior b = 0, bool preempt = true) : 
            BehaviorAbstract(n), 
            mPan(pan), 
            mTilt(tilt), 
            mLift(lift),
            mOther(b),
            mToPreempt(preempt)
        { }

    virtual void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

protected:
    int mPan;
    int mTilt;
    int mLift;
    Behavior mOther;

    bool mIsPanActive;
    bool mToPreempt;

    void setServos(RobotControl &rc);
};
//======================================================================================================================
// Behavior to pan the camera 360 degrees. It pans to the right until pointing backwards and then pans to the left until
// pointing backward. If not preempted by another behavior during this it stops and lets the next behavior execute
// 
class ServoPan : public ServoMove
{
public:
    ServoPan(Behavior b = 0, bool preempt = true) :  
      ServoMove("ServoPan", PAN_BACK_RIGHT, TILT_HORIZ, LIFT_UP, b, preempt), mIsPanningRight(true) { }

    void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);

    virtual void reset();

private:
    bool mIsPanningRight;
};
#endif // Servo_Include