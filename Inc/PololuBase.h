#ifndef PololuBase_Include
#define PololuBase_Include
//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//

class SerialPort;
#include "Crc7.h"

class PololuBase {
public:
    PololuBase(unsigned char  const device, SerialPort& sp) :
        mDevice(device), mSerialPort(sp) {
    }
    inline void sendMsg(unsigned char  msg[], unsigned char  const count) const;

    inline unsigned char  const Device() const {
        return mDevice;
    }

protected:
    inline void value_to_msg(unsigned char  msg[2], size_t const value) const;

    unsigned char  const mDevice;
    SerialPort& mSerialPort;
    Crc7 const mCrc;

    static unsigned char  const START_unsigned char  = 0xAA;
    static unsigned char  const CRC_RESERVE = 0xFF;
};
//---------------------------------------------------------------------------------------------------------------------
inline
void PololuBase::value_to_msg(unsigned char  msg[2], size_t const value) const {
    msg[0] = static_cast<unsigned char >(value & 0x7f);
    msg[1] = static_cast<unsigned char >((value >> 7) & 0x7f);
}
//---------------------------------------------------------------------------------------------------------------------
inline
void PololuBase::sendMsg(unsigned char  msg[], unsigned char  const count) const {
    msg[count - 1] = mCrc.CRC(msg, count - 1);
//    mSerialPort.write(msg, count);
}
#endif // PololuBase_Include
