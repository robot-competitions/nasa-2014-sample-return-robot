#ifndef BehaviorThread_Include
#define BehaviorThread_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//

#include "Behaviors.h"
#include "Robot.h"
#include "Subsumption.h"
//----------------------------------------------------------------------------------------------------------------------
class BehaviorThread : public Thread
{
public:
    BehaviorThread(int& wd); 
    ~BehaviorThread() { }
    void run();

    RoboRealm& roboRealm() { return mRoboRealm; }

private:
    RoboRealm mRoboRealm;
    Robot mRobot;
    Subsumption mSubsumption;

    static Behavior mBehaviors[];
};

#endif // BehaviorThread_Include
