#ifndef trig_util_Include
#define trig_util_Include
//======================================================================================================================
// 2014 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//

#define _USE_MATH_DEFINES
#include <cmath>
using namespace std;
//======================================================================================================================
static const double M_TAU = M_PI * 2;
//----------------------------------------------------------------------------------------------------------------------
inline bool dblEq(const double& lhs, const double& rhs) { return fabs(lhs - rhs) < 0.00001; }
//----------------------------------------------------------------------------------------------------------------------
template <typename T>
inline T RadianToDegree(const T& radians) { return radians * (180.0 / M_PI); }
//----------------------------------------------------------------------------------------------------------------------
template <typename T>
inline T DegreeToRadian(const T& degrees) { return degrees * (M_PI / 180.0); }
//----------------------------------------------------------------------------------------------------------------------

#endif // trig_util_Include