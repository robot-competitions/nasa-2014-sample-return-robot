#ifndef Move_Include
#define Move_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//

//======================================================================================================================
//  Behavior to control movement of the robots via motor speed control. The base class with default parameters provides movement 
//  at standard speed, a cruise behavior. Specific speeds can be specified for other situations and provide a descriptive 
//  name for logging the behavior.
//  
//  Subclasses can add more complexity by overriding operator().
//  
class Move : public BehaviorAbstract, protected Timer, public ActiveMixIn
{
public:
    // default for "cruise" which just moves straight forward
    Move() : 
        BehaviorAbstract("Cruise"), 
        Timer(NO_WAIT), 
        mLeft(Configuration::mStandardSpeed), 
        mRight(Configuration::mStandardSpeed)
        { }

    // move straight forward or back
    Move(const c_char* n, const int speed, const int period) : 
            BehaviorAbstract(n), 
            Timer(period), 
            mLeft(speed), 
            mRight(speed)
            { }

    // move in a curve
    Move(const c_char* n, const int left, const int right, const int period) : 
        BehaviorAbstract(n), 
        Timer(period), 
        mLeft(left), 
        mRight(right)
        { }

    void reset();
    virtual void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);


private:
    const int mLeft;
    const int mRight;
};

#endif // Move_Include