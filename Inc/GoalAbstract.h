#ifndef GoalAbstract_Include
#define GoalAbstract_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//=====================================================================================================================
class GoalAbstract
{
public:
    GoalAbstract(char* name, TaskList tl char* rr_fname) : mName(name), mTask(tl, array_end(tl)) { }
    //virtual void setupGoal(Subsumption& s) = 0;
    //const TaskVector& task() const { return mTask; }


protected:
	TaskVector mTask;

private:
    char* mName;
    char* rr_fname;

};
#endif // GoalAbstract_Include