#ifndef SerialPort_Include
#define SerialPort_Include
//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//

#include "ProjectStd.h"
//=============================================================================
class SerialPort {

public:
    SerialPort(const char* port_name, unsigned int baud = CBR_115200);
    ~SerialPort();

    void flush();

    unsigned char  read() const;;
    unsigned long read(unsigned char  buffer[], const unsigned int len = 1) const;

    unsigned int write(const unsigned char  buffer[], const unsigned int cnt) const;
    void write(unsigned char  ch) const;
    void write(const unsigned char  buffer[]) const { write(buffer+1, buffer[0]); }

    const char* port() const;

    const unsigned int isRxReady() volatile;
    void waitRxReady();

private:
    void* mPort;
    char mPortName[8];
};
//----------------------------------------------------------------------------------------------------------------------
inline
void SerialPort::waitRxReady() 
{
    while (!isRxReady())
    {
        ::Sleep(0);
    }
}
//-----------------------------------------------------------------------------
inline
const char* SerialPort::port() const
{
    return mPortName;
}
//----------------------------------------------------------------------------------------------------------------------
inline
unsigned char  SerialPort::read() const
{
    unsigned char  buf;
    read(&buf);
    return buf;
}
//-----------------------------------------------------------------------------
inline
void SerialPort::write(unsigned char  ch) const
{
    write(&ch, 1);
}

#endif