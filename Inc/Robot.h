#ifndef Thumper_Include
#define Thumper_Include
//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "Maestro.h"
#include "PololuSimple.h"
#include "RoboRealm.h"
#include "RobotData.h"
#include "RobotControl.h"
//======================================================================================================================
class Robot
{
public:
    Robot(RoboRealm& rr);
    ~Robot();

    void controlUpdate(const RobotControl& c);
    const RobotData getData() { acquireData(); return mRobotData; }
    void updatePipeline(string& p);

protected:

    void acquireData();
    void acquireMaestroData();
    void acquireRRData();
    void acquireSimpleMCData(PololuSimple& ps, SimpleMCData& s, char* side);
    void calculateRcData();

    void controlMaestro();
    void controlPause();
    void controlSpeed();

    void emitSpeed(PololuSimple& ps, const int s);
    void limitSpeed(int& v, int controller_limit = 100);

    RobotData mRobotData;
    RobotControl mRobotControl;

    SimpleMCData& mLSimpleData;
    SimpleMCData& mRSimpleData;
    RcData& mRcData;

    bool mInhibitDrive;

//    SerialPort mLSerial;
//    SerialPort mRSerial;
//    SerialPort mMSerial;
    Maestro mMaestro;

    PololuSimple mLeftDrive;
    PololuSimple mRightDrive;

    RoboRealm& mRoboRealm;
};

#endif // Thumper_Include
