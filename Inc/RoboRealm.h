#ifndef RoboRealm_Include
#define RoboRealm_Include
//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//#include <string>
using namespace std;

//#include "RoboRealmVars.h"
#include "RobotData.h"
#include "RobotControl.h"
//======================================================================================================================
class RoboRealm
{
public:
    RoboRealm(char* rr_exe, int rr_port, char* rr_prog);
    ~RoboRealm();

    void init();
    int read();
    void write();

    void getData(RobotData& r);
    void setData(RobotControl& rc);
    void setScript(string& pipeline) { mScript = pipeline; }

protected:

private:
    void executeScript();

    char* mRoboRealmExe;
    int mRoboRealmPort;
    char* mRoboRealmProg;
    string mScript;

//    RR_API mRR_API;
//
//    // variables with values from RR
//    rrIntVar mOrangeFence;
//    rrIntVar mPosX;
//    rrIntVar mPosY;
//
//    rrStrVar mLabel;
//    rrIntVar mDistance;
//    rrIntVar mConfidence;
//
//    RoboRealmVars mGetVars;  // be sure is last in list so initialized after vars
//
//    // variables with values for RR
//    rrIntVar mLeftSpeed;
//    rrIntVar mRightSpeed;
//    rrIntVar mRetarget;
//    RoboRealmVars mSetVars;  // be sure is last in list so initialized after vars

};

#endif // RoboRealm_Include
