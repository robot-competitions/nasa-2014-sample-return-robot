#ifndef SRR_Include
#define SRR_Include
//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//  SRR is the application. It constructs all the required classes as static objects. 
//  
//  It creates the operating threads and monitors their watchdog counters to stop execution if a thread hangs. The 
//  overall batch file will then restart the entire application. 
//  
//  SRR also watches for the 'break' file to be completed and stops excution if that happens.
//======================================================================================================================
#include "RRThread.h"
#include "BehaviorThread.h"
#include "PauseThread.h"
//======================================================================================================================
class SRR
{
public:
	SRR();
	~SRR();

    void operator()();

private:
    bool fileCheck(c_char* fname);
    void watchdogCheck();
    void watchdog(int& watchdog, int limit, string text);

    int mBWatchDog;
    int mRRWatchDog;
    int mPauseDog;

    BehaviorThread mBehaviorThread;
    RRThread mRRThread;
    //PauseThread mPauseThread;
};
#endif // SRR_Include
