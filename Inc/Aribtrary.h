#ifndef Aribtrary_Include
#define Aribtrary_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
// Provide routines for making arbitrary choices (sounds better than random)
// 
#define _CRT_RAND_S
#include <stdlib.h>

inline size_t arbitraryValue() { size_t rv; rand_s(&rv); return rv;}
inline size_t arbitraryRange(const size_t low_limit, const size_t hi_limit) { return (arbitraryValue() % hi_limit) + low_limit;  }
inline size_t arbitraryLimited(const size_t hi_limit) { return arbitraryRange(0, hi_limit); }
inline bool arbitraryBool() { return (arbitraryValue() & 0x200) != 0; }


#endif // Aribtrary_Include