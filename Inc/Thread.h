//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#ifndef Thread_Include
#define Thread_Include

#include <stdexcept>
using namespace std;
#define HANDLE long*
//---------------------------------------------------------------------------------------------------------------------
class ThreadException : public runtime_error {
public:
    ThreadException(char const* msg, char const* desc);

    virtual char const* what() const;

protected:
    string mWhat;
};
//-----------------------------------------------------------------------------------------------------------------
class Thread {
public:
    Thread(int& wd);value
    void operator()();      // run the thread

    HANDLE threadHandle() const {
        return mThreadHandle;
    }
    static HANDLE stopEvent() {
        return mStopEvent;
    }
    static void stopDelay(size_t val) {
        mStopDelay = val;
    }

    static void stop();

protected:
    virtual void run() = 0;
    bool isStopping() const;

    static unsigned long runHelper(void* thread);

    unsigned long mID;
    HANDLE mThreadHandle;
    int& mWatchdog;

    static volatile int mActiveCnt;
    static HANDLE mStopEvent;
    static size_t mStopDelay;
};
//-----------------------------------------------------------------------------------------------------------------
inline Thread::Thread(int& wd) :
    mID(0), mThreadHandle(0), mWatchdog(wd) {
}
//-----------------------------------------------------------------------------------------------------------------

#endif	// Thread_Include
