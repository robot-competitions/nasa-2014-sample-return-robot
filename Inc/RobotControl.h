#ifndef RobotControl_Include
#define RobotControl_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
struct RobotControl {
//    static RobotControl robotcontrol() {}

    RobotControl() :
        mLeftSpeed(0),              // no movement
        mRightSpeed(0), mRetarget(false), mSafeReset(false), mPan(1540),                 // forward
        mTilt(1540),                // horizontal
        mLift(2000),                // up
        mActivatePause(false), mDeactivatePause(false), mSafetyLED(true)            // on
    {
    }

    //  Drive speed
    int mLeftSpeed;
    int mRightSpeed;
    bool mRetarget;
    bool mSafeReset;

    // Servo and LED control
    bool mSafetyLED;
    int mPan;
    int mTilt;
    int mLift;

    // Pause control
    bool mActivatePause;
    bool mDeactivatePause;
};
#endif // RobotControl_Include
