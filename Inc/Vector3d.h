#ifndef Vector3d_Include
#define Vector3d_Include
//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <cmath>
#include <vector>
#include <iomanip>
using namespace std;

static const size_t PRECISION = 3;
static const size_t WIDTH = PRECISION+6;
//======================================================================================================================
class Vector3d: public vector<double>
{
    friend Vector3d Vector3d::operator+(const Vector3d& lhs, const Vector3d& rhs);
    friend Vector3d Vector3d::operator-(const Vector3d& lhs, const Vector3d& rhs);
    friend Vector3d Vector3d::operator*(const Vector3d& lhs, const Vector3d& rhs);
    friend ostream& operator<<(ostream& os, const Vector3d& v);

public:
    Vector3d() { assign(3, 0); }
    Vector3d(const double& one, const double& two, const double& three);
    Vector3d(const double begin[3]);
    Vector3d(const Vector3d& v);

    double modulus() const;
    Vector3d& normalize();

    Vector3d operator-() const { return *this * -1.0; }

    Vector3d operator+(const double& scalar) const;

    Vector3d operator*(const double& scalar) const;
    Vector3d operator/(const double& scalar) const;

    Vector3d operator*=(const double& scalar);
    Vector3d operator/=(const double& scalar);

    Vector3d operator=(const double& scalar);

    Vector3d operator+(const Vector3d& rhs);
    Vector3d operator+=(const Vector3d& rhs);
    Vector3d operator-(const Vector3d& rhs);
    Vector3d operator-=(const Vector3d& rhs);
    Vector3d operator*(const Vector3d& rhs);
    Vector3d operator*=(const Vector3d& rhs);
    Vector3d operator/(const Vector3d& rhs);
    Vector3d operator/=(const Vector3d& rhs);

    double dot(const Vector3d& rhs) const;

    double X() const { return (*this)[0]; }
    double Y() const { return (*this)[1]; }
    double Z() const { return (*this)[2]; }

    inline void dup(Vector3d& src, Vector3d& dest){ copy(src.begin(), src.end(), dest.begin()); }

    inline void display(ostream& os) const;
    static double Vector3d::dot(const Vector3d& lhs, const Vector3d& rhs);
    Vector3d angles() const;

private:

};
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d::Vector3d(const double& one, const double& two, const double& three): vector(3) {
    vector& me = *this;
    me[0] = one; me[1] = two; me[2] = three;
}
//----------------------------------------------------------------------------------------------------------------------
inline
Vector3d::Vector3d(const double begin[3])
{
    assign(begin, begin+3);
}
//----------------------------------------------------------------------------------------------------------------------
inline
Vector3d::Vector3d(const Vector3d& v)
{
    assign(v.begin(), v.end());
}
//======================================================================================================================
//  Assignment
inline Vector3d Vector3d::operator=(const double& scalar){
    Vector3d& res = *this;
    res[0] = scalar;
    res[1] = scalar;
    res[2] = scalar;
    return res;
}
//----------------------------------------------------------------------------------------------------------------------
// Scalar Mult & Div operators
inline Vector3d Vector3d::operator*(const double& scalar) const {
    Vector3d res = *this;
    res[0] *= scalar;
    res[1] *= scalar;
    res[2] *= scalar;
    return res;
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d Vector3d::operator*=(const double& scalar) {
    *this = *this * scalar;
    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d Vector3d::operator/(const double& scalar) const{
    return *this * ( 1 / scalar);
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d Vector3d::operator/=(const double& scalar) {
    *this *= (1 / scalar);
    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
// Scalar Addition & Subtraction operators
inline Vector3d Vector3d::operator+(const double& scalar) const {
    Vector3d res = *this;
    res[0] += scalar;
    res[1] += scalar;
    res[2] += scalar;
    return res;
}
//----------------------------------------------------------------------------------------------------------------------
// Vector Addition & Subtraction operators
inline Vector3d Vector3d::operator+(const Vector3d& rhs){
    Vector3d me = *this;
    me[0] += rhs[0];
    me[1] += rhs[1];
    me[2] += rhs[2];
    return me;
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d operator+(const Vector3d& lhs, const Vector3d& rhs){
    Vector3d temp = lhs;
    return temp + rhs;
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d Vector3d::operator+=(const Vector3d& rhs){
    Vector3d& me = *this;
    me = me + rhs;
    return me;
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d Vector3d::operator-(const Vector3d& rhs){
    Vector3d me = *this;
    me[0] -= rhs[0];
    me[1] -= rhs[1];
    me[2] -= rhs[2];
    return me;
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d operator-(const Vector3d& lhs, const Vector3d& rhs){
    Vector3d temp = lhs;
    return temp - rhs;
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d Vector3d::operator-=(const Vector3d& rhs) {
    Vector3d& me = *this;
    me = me - rhs;
    return me;
}
//----------------------------------------------------------------------------------------------------------------------
inline
Vector3d Vector3d::operator*(const Vector3d& rhs)
{
    Vector3d me = *this;
    me[0] *= rhs[0];
    me[1] *= rhs[1]; 
    me[2] *= rhs[2];
    return me;
}
//----------------------------------------------------------------------------------------------------------------------
inline Vector3d operator*(const Vector3d& lhs, const Vector3d& rhs) {
    Vector3d temp = lhs;
    return temp * rhs;
}
//----------------------------------------------------------------------------------------------------------------------
inline
void Vector3d::display(ostream& os) const
{
    const Vector3d& me = *this;
    os << setiosflags( ios::fixed ) << setprecision(PRECISION) << showpoint;

    for (int i = 0; i < 3; i++) {
        os << setw(WIDTH) << me[i];
    }
}
//----------------------------------------------------------------------------------------------------------------------
inline ostream& operator<<(ostream& os, const Vector3d& v)
{
    v.display(os);
    return os;
}
//----------------------------------------------------------------------------------------------------------------------
//get modulus of a 3d mV sqrt(x^2+y^2+y^2)
inline double Vector3d::modulus() const {
    const Vector3d& mV = *this;
    double R;
    R = mV[0]*mV[0];
    R += mV[1]*mV[1];
    R += mV[2]*mV[2];
    return sqrt(R);
}
//----------------------------------------------------------------------------------------------------------------------
//convert mV to a mV with same direction and modulus 1
inline Vector3d& Vector3d::normalize(){
    Vector3d& mV = *this;
    double R = modulus();
    mV[0] /= R;
    mV[1] /= R;
    mV[2] /= R;
    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
//calculate mV dot-product c = a . b
inline
double Vector3d::dot(const Vector3d& lhs, const Vector3d& rhs){
    return lhs[0] * rhs[0] + lhs[1] * rhs[1] + lhs[2] * rhs[2];
}
//----------------------------------------------------------------------------------------------------------------------
//calculate mV dot-product c = a . b
inline
double Vector3d::dot(const Vector3d& rhs) const {
    const Vector3d& lhs = *this;
    return lhs[0] * rhs[0] + lhs[1] * rhs[1] + lhs[2] * rhs[2];
}
//---------------------------------------------------------------------------------------------------------------------
//  calculate angles of axis
inline
Vector3d Vector3d::angles() const
{
    const Vector3d& v = *this;
    const double& x = v[0];
    const double& y = v[1];
    const double& z = v[2];

    Vector3d angles;

    // all the different ways of calculating the angles from the acceleraction vector
    //angles[0] = asin(x);   // 0 -30
    //angles[1] = asin(y);   // -0 -30

    //angles[0] = atan2(-x, z);    // 0 30
    //angles[1] = atan2(-y, z);    // 0 30

    //angles[0] = atan2(z, x);    // 90 120
    //angles[1] = atan2(z, y);    // 90 120

    //angles[0] = atan2(-x, sqrt(y*y + z*z));   // 0 30
    //angles[1] = atan2(-y, sqrt(x*x + z*z));   // 0 30
    angles[2] = atan2(z, sqrt(x*x + y*y));    // -90 -60

    angles[0] = atan2(sqrt(y*y + z*z), x);   // 90 120
    angles[1] = atan2(sqrt(x*x + z*z), y);   // 90 120
    //angles[2] = atan2(sqrt(x*x + y*y), z);   //0 30

    //angles[2] = asin(z);    // -90 -60
    //angles[2] = acos(z);    // 180 150       c-up 0 30   0 is fp error

    // these give 90 for x,y with z==0 when horizontal and positive when tipped above the horizon
    angles[0] = atan2(x, sqrt(y*y + z*z));   // 0 30
    angles[1] = atan2(y, sqrt(x*x + z*z));   // 0 30
    //angles[2] = atan2(sqrt(x*x + y*y), z);   // 0 30


    return angles;
}

#if 0
//----------------------------------------------------------------------------------------------------------------------
//calculate mV cross-product c = a x b
void Vector3d::cross(double* a,double* b, double* c){
 c[0] = a[1]*b[2] - a[2]*b[1];
 c[1] = a[2]*b[0] - a[0]*b[2];
 c[2] = a[0]*b[1] - a[1]*b[0];
}
#endif

#endif // Vector3d_Include
