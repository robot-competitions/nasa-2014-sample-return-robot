#ifndef RobotData_Include
#define RobotData_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//----------------------------------------------------------------------------------------------------------------------
struct SimpleMCData {
    bool mIsFaulted;
    bool mIsLowVin;
    bool is_kill;
    bool low_vin;

    size_t err_occ;
    size_t limit;
    size_t temp;
    float volts;
    size_t analog1;
    int rc1_in;
    int rc2_in;
    int targ;
};
//----------------------------------------------------------------------------------------------------------------------
// RC data as percentage of full scale (-3200 to 3200)
struct RcData {
    int mRc1;
    int mRc2;
    int mRc3;
};
//----------------------------------------------------------------------------------------------------------------------
enum eButtons { A=0, B, C, D };
//----------------------------------------------------------------------------------------------------------------------
struct RobotData {
    //  RF / Pause button inputs
    bool mIsPaused;

    bool mButtons[4];

    //  Servo status
    bool mIsMoving;
    size_t mPanPos;
    size_t mTiltPos;
    size_t mLiftPos;

    // Motor status
    SimpleMCData mLeftDrive;
    SimpleMCData mRightDrive;

    // RC Data
    RcData mRcData;

    // RoboRealm Image Data
    bool mIsOrangeFence;
    string mName;
    int mPosX;
    int mPosY;
    int mDistance;
    int mConfidence;

};
#endif // RobotData_Include
