#ifndef FindBase_Include
#define FindBase_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
struct ImageLocation {
    ImageLocation() : 
        mPan(), 
        mPosX(), 
        mPosY(),
        mFound(false) 
        {}

    size_t mPan;
    int mPosX;
    int mPosY;
    bool mFound;
};
//======================================================================================================================
//  Behavior to recognize when the RR pipeline locates the position of the base
//  Works with ServoPan behavior that rotates the servos
//  
class FindImage : public BehaviorAbstract, public ActiveMixIn, public IndirectMixIn
{
public:
    FindImage(Behavior b = 0) : BehaviorAbstract("FindImage"), IndirectMixIn(b) { }

    virtual void operator()(RobotData& rd, RobotControl& rc, Subsumption& sub);

    virtual void reset();

protected:
    int mPan;
    bool mIsPanStopped;
};
//======================================================================================================================
//  Behavior to recognize when the RR pipeline locates the position of the base
//   to turn the rover toward the base
//  
class TurnToImage : public BehaviorAbstract, public ActiveMixIn
{
public:
    TurnToImage(BehaviorAbstract& r, BehaviorAbstract& f, BehaviorAbstract& l) : 
        BehaviorAbstract("TurnToBase"),
        mRight(r),
        mForward(f),
        mLeft(l)
        { }

    virtual void operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption);
    void reset();
protected:
    BehaviorAbstract& mRight;
    BehaviorAbstract& mForward;
    BehaviorAbstract& mLeft;

};
#endif // FindBase_Include