#ifndef Configuration_Include
#define Configuration_Include
//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "ProjectStd.h"
//======================================================================================================================
class Configuration
{
public:
   Configuration();

    void load();    // read robot.ini file

    // configuration values loaded from robot.ini file

    static char mFirstTask[20];
    static int mUpdateRate;

    static char mLComPort[7];
    static size_t mLDevice;

    static char mRComPort[7];
    static size_t mRDevice;

    static char mMaestroComPort[7];
    static char mTTLComPort[7];
    static size_t mMaestroDevice;

    static bool mInhibitDrive;
    static bool mInhibitRC;
    
    static char mRoboRealmExec[255];
    static int mRoboRealmPort;
    static char mRoboRealmProg[255];

    // configuration values that are static constants
    static const int mHighSpeed = 100; //  percent
    static const int mStandardSpeed = int(mHighSpeed * 0.80);
    static const int mMidSpeed = int(mHighSpeed * 0.60);
    static const int mSlowSpeed = int(mHighSpeed * 0.40);
    static const int mCrawlSpeed = int(mHighSpeed * 0.20);

    static int mWatchDogLimit;
    static const int mWatchDogTimeout = 1500;

    static const size_t mImageHeight = 240;
    static const size_t mImageWidth = 424;
    static const size_t mImageCenter = mImageWidth / 2;

protected:

private:

};
#endif // Configuration_Include
