#ifndef operators_Include
#define operators_Include
//======================================================================================================================
// 2014 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//  Define templates for standard operators that can use the 'op'= format or other logical tests 
//  This provides operators for types without having to write them for all types
//  
//======================================================================================================================
template <typename T, typename S>
struct operators {
    //virtual const T& operator+=(const T& rhs) = 0;
    //virtual const T& operator-=(const T& rhs) = 0;
    virtual const T& operator*=(const T& rhs) = 0;
    //virtual const T& operator/=(const T& rhs) = 0;
     
};
//----------------------------------------------------------------------------------------------------------------------
template <typename T, typename S> inline 
const T operator+(const S& s, const T& t) { return Quaternion(t) *= s; }
//----------------------------------------------------------------------------------------------------------------------
template <typename T, typename S> inline 
const T operator-(const S& s, const T& t) { return Quaternion(t) *= s; }
//----------------------------------------------------------------------------------------------------------------------
template <typename T, typename S> inline 
const T operator*(const S& s, const T& t) { return Quaternion(t) *= s; }
//----------------------------------------------------------------------------------------------------------------------
template <typename T, typename S> inline 
const T operator/(const S& s, const T& t) { return Quaternion(t) *= s; }
//----------------------------------------------------------------------------------------------------------------------
template <typename T>
inline
bool operator!=(const T& lhs, const T& rhs) { return !(lhs == rhs); }


//template <typename T, typename S>
//inline
//const T operator*(const T& t, const S& s) { return t * s; }

#endif // operators_Include