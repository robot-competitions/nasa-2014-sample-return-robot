#ifndef Crc7_Include
#define Crc7_Include
//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//

#include "ProjectStd.h"

class Crc7 {
public:
    Crc7();

    unsigned char  CRC(unsigned char message[], unsigned char length) const;

private:
    static unsigned char  GetCRC(unsigned char val);
    static void GenerateCRCTable();

    static unsigned char  const CRC7_POLY = 0x91;
    static unsigned char  CRCTable[256];

    static bool mGenerated;
};

#endif // Crc7_Include
