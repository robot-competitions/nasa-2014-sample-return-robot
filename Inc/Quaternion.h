#ifndef Quaternion_Include
#define Quaternion_Include
//======================================================================================================================
// 2014 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "trig_util.h"
#include "Vector3d.h"
//---------------------------------------------------------------------------------------------------------------------
class Quaternion
{
public:
    // constructor to set angles
    Quaternion(const double pitch, const double roll, const double yaw);
    Quaternion(const Vector3d& v);
    Quaternion(const Quaternion& q);

    // these constructors set raw values, not angles
    Quaternion() : mScalar(), mVect(), w(mScalar), x(mVect[0]), y(mVect[1]), z(mVect[2]) {}
    Quaternion(const double w, const double x, const double y, const double z);
    Quaternion(const double& s, const Vector3d& v);

    // access methods
    double scalar() const { return mScalar; }
    const Vector3d& vect() const { return mVect; }

    double X() const { return this->mVect[0]; }
    double Y() const { return this->mVect[1]; }
    double Z() const { return this->mVect[2]; }
    double W() const { return this->mScalar; }

    // conversions
    Vector3d angles() const;
    Vector3d angles_deg() const { return RadianToDegree(angles()); }

    // equivalence testing
    bool isIdentical(const Quaternion& rhs) const;  // parallel but not a dual
    bool isDual(const Quaternion& rhs) const;       // parallel but maybe be duals

            // || is dual, i.e. equal angle but different values. 
    bool operator||(const Quaternion& rhs) const { return isDual(rhs); }

    // graphic operations
    Quaternion rotateBy(Quaternion q_gyro) const { return *this * q_gyro; }

    // 'vector' operations
    Quaternion conj() const { return Quaternion(mScalar, -mVect); }
    Quaternion diff(const Quaternion& q) const { return this->inv() * q; }
    double dot(const Quaternion& rhs) const;
    Quaternion inner(const Quaternion& q) const;
    Quaternion inv() const { return conj() / norm();}

    double magnitude() const { return sqrt(norm()); }
    double norm() const;
    Quaternion& normalize();
    
    const Quaternion operator-() const { return *this * -1.0; }
    void operator=(const Quaternion& rhs);

    // scalar operators
    const Quaternion operator+(const double& rhs) const { return Quaternion(*this) += rhs; }
    const Quaternion operator-(const double& rhs) const { return Quaternion(*this) -= rhs; }
    const Quaternion operator*(const double& rhs) const { return Quaternion(*this) *= rhs; }
    const Quaternion operator/(const double& rhs) const { return Quaternion(*this) /= rhs; }

    const Quaternion& operator+=(const double& rhs) { w+=rhs; x+=rhs; y+=rhs; z+=rhs; return *this; }
    const Quaternion& operator-=(const double& rhs) { w-=rhs; x-=rhs; y-=rhs; z-=rhs; return *this; }
    const Quaternion& operator*=(const double& rhs) { w*=rhs; x*=rhs; y*=rhs; z*=rhs; return *this; }
    const Quaternion& operator/=(const double& rhs) { w/=rhs; x/=rhs; y/=rhs; z/=rhs; return *this; }

    // Quaternion operators
    const Quaternion operator+(const Quaternion& rhs) const { return Quaternion(*this) += rhs; }

    const Quaternion& operator+=(const Quaternion& rhs) { w+=rhs.w, x+=rhs.x; y+=rhs.y, z+=rhs.z; return *this; }

    const Quaternion operator*(const Quaternion& rhs) const { return Quaternion(*this) *= rhs; }
    const Quaternion& operator*=(const Quaternion& rhs);

    bool operator==(const Quaternion& rhs) const { return dblEq(w,rhs.w) && dblEq(x, rhs.x) && dblEq(y, rhs.y) && dblEq(z, rhs.z); }
    bool operator!=(const Quaternion& rhs) const { return !(*this == rhs); }

    friend ostream& operator<<(ostream& os, const Quaternion& q);

private:
    void display(ostream& os) const;
    void setQuaternion(double x, double y, double z);
    void justify(double& ax) const;

    double mScalar; 
    Vector3d mVect; 

    double& w;
    double& x;
    double& y;
    double& z;
};
//======================================================================================================================
inline
ostream& operator<<(ostream& os, const Quaternion& q) {
    q.display(os);
    return os;
}
//----------------------------------------------------------------------------------------------------------------------
inline const Quaternion operator*(const double& lhs, const Quaternion& rhs) { return Quaternion(rhs) *= lhs; }


#endif // Quaternion_Include
