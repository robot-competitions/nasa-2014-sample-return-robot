#ifndef Goal_Include
#define Goal_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//----------------------------------------------------------------------------------------------------------------------
typedef char* RRFName;
typedef RRFName RRList[];
//======================================================================================================================
class Goal 
{
public:
    Goal(char* n, TaskList tl, RRList rrl);

    char* name() { return mName; }
    string pipeline() { return mPipeline; }
    TaskVector& tasks() { return mTasks; }

private:
    void buildPipeline(RRList fnames);
    char* mName;
    TaskVector mTasks;
    string mPipeline;       // script for vision processing pipeline of RoboRealm
};

#endif // Goal_Include