#ifndef ButtonsImpl_Include
#define ButtonsImpl_Include
//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
template <int key>
void Buttons<key>::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption)
{
    if (!mPreempted) {
        if (rd.mButtons[key]) {
            mBehavior(rd, rc, mSubsumption);
        }
    }
}

#endif // ButtonsImpl_Include