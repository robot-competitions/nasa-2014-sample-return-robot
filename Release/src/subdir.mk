################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/BehaviorAbstract.cpp \
../src/BehaviorThread.cpp \
../src/Buttons.cpp \
../src/Configuration.cpp \
../src/Crc7.cpp \
../src/DoBehavior.cpp \
../src/FindImage.cpp \
../src/Flee.cpp \
../src/Goal.cpp \
../src/Goals.cpp \
../src/LED.cpp \
../src/Maestro.cpp \
../src/Move.cpp \
../src/NewGoal.cpp \
../src/OrangeFence.cpp \
../src/PIDControl.cpp \
../src/Panic.cpp \
../src/Pause.cpp \
../src/PauseThread.cpp \
../src/PololuBase.cpp \
../src/PololuSimple.cpp \
../src/RCControl.cpp \
../src/RRThread.cpp \
../src/RoboRealm.cpp \
../src/Robot.cpp \
../src/SRR.cpp \
../src/ServoMove.cpp \
../src/Startup.cpp \
../src/Subsumption.cpp \
../src/Track.cpp \
../src/TrackFiducial.cpp \
../src/Trapped.cpp \
../src/main.cpp 

OBJS += \
./src/BehaviorAbstract.o \
./src/BehaviorThread.o \
./src/Buttons.o \
./src/Configuration.o \
./src/Crc7.o \
./src/DoBehavior.o \
./src/FindImage.o \
./src/Flee.o \
./src/Goal.o \
./src/Goals.o \
./src/LED.o \
./src/Maestro.o \
./src/Move.o \
./src/NewGoal.o \
./src/OrangeFence.o \
./src/PIDControl.o \
./src/Panic.o \
./src/Pause.o \
./src/PauseThread.o \
./src/PololuBase.o \
./src/PololuSimple.o \
./src/RCControl.o \
./src/RRThread.o \
./src/RoboRealm.o \
./src/Robot.o \
./src/SRR.o \
./src/ServoMove.o \
./src/Startup.o \
./src/Subsumption.o \
./src/Track.o \
./src/TrackFiducial.o \
./src/Trapped.o \
./src/main.o 

CPP_DEPS += \
./src/BehaviorAbstract.d \
./src/BehaviorThread.d \
./src/Buttons.d \
./src/Configuration.d \
./src/Crc7.d \
./src/DoBehavior.d \
./src/FindImage.d \
./src/Flee.d \
./src/Goal.d \
./src/Goals.d \
./src/LED.d \
./src/Maestro.d \
./src/Move.d \
./src/NewGoal.d \
./src/OrangeFence.d \
./src/PIDControl.d \
./src/Panic.d \
./src/Pause.d \
./src/PauseThread.d \
./src/PololuBase.d \
./src/PololuSimple.d \
./src/RCControl.d \
./src/RRThread.d \
./src/RoboRealm.d \
./src/Robot.d \
./src/SRR.d \
./src/ServoMove.d \
./src/Startup.d \
./src/Subsumption.d \
./src/Track.d \
./src/TrackFiducial.d \
./src/Trapped.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++20 -I"/mnt/devel/Archive/SampleReturn/Inc" -O3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


