//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//

#include "Crc7.h"

unsigned char  Crc7::CRCTable[256];
bool Crc7::mGenerated = false;
//---------------------------------------------------------------------------------------------------------------------
Crc7::Crc7()
{
    if (mGenerated) return;
    GenerateCRCTable(); 
    mGenerated = true;
}
//---------------------------------------------------------------------------------------------------------------------
unsigned char  Crc7::CRC(unsigned char message[], unsigned char length) const
{
    unsigned char  i, crc = 0;

    for (i = 0; i < length; i++)
    {
        crc = CRCTable[crc ^ message[i]];
    }
    return crc;
}
//---------------------------------------------------------------------------------------------------------------------
unsigned char  Crc7::GetCRC(unsigned char val)
{
    unsigned char  j;

    for (j = 0; j < 8; j++)
    {
        if (val & 1)
            val ^= CRC7_POLY;
        val >>= 1;
    }

    return val;
}
//---------------------------------------------------------------------------------------------------------------------
void Crc7::GenerateCRCTable()
{
    int i;

    // generate a table value for all 256 possible unsigned char  values
    for (i = 0; i < 256; i++)
    {
        CRCTable[i] = GetCRC(i);
    }
}
