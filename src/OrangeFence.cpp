//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;

#include "ProjectStd.h"
#include "Journal.h"

#include "Configuration.h"
#include "Behaviors.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
void OrangeFence::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) 
{ 
    detail("OrangeFence");
    if (!mPreempted)
    {
        if (rd.mIsOrangeFence) {
            rc.mLeftSpeed = Configuration::mSlowSpeed;
            rc.mRightSpeed = Configuration::mHighSpeed;
            detail("OrangeFence", "L", rc.mLeftSpeed, "R", rc.mRightSpeed);
            mPreempted = true;
        }
    }
}
