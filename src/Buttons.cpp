//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "ProjectStd.h"
#include "Journal.h"
#include "Configuration.h"
#include "Behaviors.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
void CButton::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption)
{
    detail("CButton");

    if (!mPreempted) {
        if (rd.mButtons[C]) {
            mBehavior(rd, rc, mSubsumption);
        }
    }
}
//----------------------------------------------------------------------------------------------------------------------
void DButton::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption)
{
    detail("DButton");

    if (!mPreempted) {
        if (rd.mButtons[D]) {
            mBehavior(rd, rc, mSubsumption);
        }
    }
}
