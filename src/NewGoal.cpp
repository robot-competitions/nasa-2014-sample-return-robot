//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <fstream>

#include "Configuration.h"
#include "Behaviors.h"
#include "Subsumption.h"
#include "Goal.h"
//----------------------------------------------------------------------------------------------------------------------
// redefine speed settings to shorten their use with creating Moves
static const int highSpeed = Configuration::mHighSpeed;
static const int standardSpeed = Configuration::mStandardSpeed;
static const int slowSpeed = Configuration::mSlowSpeed;
//======================================================================================================================
NewGoal::NewGoal(Goal& tv) :  BehaviorAbstract("Goal"), mGoal(tv) {  }
//----------------------------------------------------------------------------------------------------------------------
void NewGoal::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption)
{
    if (!mPreempted) {
        mPreempted = true;
        mSubsumption.nextGoal(mGoal);
    }   
}
