//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//
#include "PololuSimple.h"
#include "Journal.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
PololuSimple::PololuSimple(const unsigned char  device, SerialPort& sp) : PololuBase(device, sp)
{
    exitSafeStart();
}
//----------------------------------------------------------------------------------------------------------------------
PololuSimple::~PololuSimple()
{
    detail("Stopping motor");
    stop();
}
//---------------------------------------------------------------------------------------------------------------------
size_t PololuSimple::getErrorOccurred() { 
    size_t err = getVariable(ERROR_OCCURED_REG); 

    mIsSafeStartErr = (err & SAFE_START) != 0;
    mIsKillSw = (err & KILL_SWITCH )!= 0;
    mIsOverTemp = (err & OVER_TEMP) != 0;
    mIsLowVin = (err & LOW_VIN) != 0;

    return err;
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::version(unsigned char  version[4]) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, GET_VER_CODE, CRC_RESERVE};

    sendMsg(msg, sizeof(msg));
    mSerialPort.waitRxReady();
    mSerialPort.read(version, 4);
}
//----------------------------------------------------------------------------------------------------------------------
int PololuSimple::getVariable(unsigned char  variable_id) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, GET_VAR_CODE, variable_id, CRC_RESERVE};

    sendMsg(msg, sizeof(msg));
    mSerialPort.waitRxReady();

    unsigned char  buf[2];
    mSerialPort.read(buf, 2);

    short value = ((buf[1] << 8) | buf[0]) & 0xFFFF;
    return value;
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::brake(const unsigned char  speed) const /* 0 to 32 */
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x12, speed & 0x1F, CRC_RESERVE};

    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::exitSafeStart() const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, SAFE_START_CODE, CRC_RESERVE};
    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::forward(const int speed) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, FORWARD_CODE, 0xFF, 0xFF, CRC_RESERVE};

    msg[3] = static_cast<unsigned char >(speed & 0x1f);
    msg[4] = static_cast<unsigned char >(speed >> 5);

    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::forwardShort(const unsigned char  speed) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, FORWARD_SHORT_CODE, speed, CRC_RESERVE};

    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::forwardPercent(const unsigned char  percent_speed) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, FORWARD_CODE, 0xFF, 0xFF, CRC_RESERVE};

    msg[3] = 0;
    msg[4] = percent_speed;

    sendMsg(msg, sizeof(msg));

//    forward(percent_speed >> 5);    // force low output unsigned char  to 0
}
//---------------------------------------------------------------------------------------------------------------------
inline
void PololuSimple::reverse(const int speed) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, REVERSE_CODE, 0xFF, 0xFF, CRC_RESERVE};

    msg[3] = static_cast<unsigned char >(speed & 0x1f);
    msg[4] = static_cast<unsigned char >(speed >> 5);

    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::reverseShort(const unsigned char  speed) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, REVERSE_SHORT_CODE, speed, CRC_RESERVE};
    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::reversePercent(const unsigned char  percent_speed) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, REVERSE_CODE, 0xFF, 0xFF, CRC_RESERVE};

    msg[3] = 0;
    msg[4] = percent_speed;

    sendMsg(msg, sizeof(msg));

    //reverse(percent_speed >> 5);    // force low output unsigned char  to 0
}
//---------------------------------------------------------------------------------------------------------------------
void PololuSimple::stop() const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x60, CRC_RESERVE};
    sendMsg(msg, sizeof(msg));
}
