#include <cstdlib>
#include "OS.h"
#include "ProjectStd.h"
#include "Journal.h"
#include "Configuration.h"
using namespace std;

//----------------------------------------------------------------------------------------------------------------------
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
int Configuration::mUpdateRate;
char Configuration::mLComPort[7];
size_t Configuration::mLDevice;
char Configuration::mRComPort[7];

size_t Configuration::mRDevice;
bool Configuration::mInhibitDrive;
bool Configuration::mInhibitRC;

char Configuration::mRoboRealmExec[255];
int Configuration::mRoboRealmPort;
char Configuration::mRoboRealmProg[255];

char Configuration::mMaestroComPort[7];
size_t Configuration::mMaestroDevice;
char Configuration::mTTLComPort[7];
char Configuration::mFirstTask[20];

int Configuration::mWatchDogLimit;
//----------------------------------------------------------------------------------------------------------------------
Configuration::Configuration()
{
    load();
    mWatchDogLimit = mWatchDogTimeout / mUpdateRate;
}
//----------------------------------------------------------------------------------------------------------------------
void Configuration::load()
{
	static c_char* fname = ".\\robot.ini";
    static c_char* section = "configuration";
    size_t cnt = 0;

    // setup journaling and use it to detect if the config file not present, i.e. mJournalLevel == 0 
    int j_level = GetPrivateProfileInt(section, "journal", 0, fname);
    if (j_level == 0)
    {
        throw runtime_error("Could not load configuration");
    }
    JournalEmit::setLevel((JournalEmit::Level)j_level);

    // get first task robots should perform - maybe overridden by specific assignment to this robot
    cnt = GetPrivateProfileString(section, "task", "", mFirstTask, sizeof mFirstTask, fname);
    journal("task", mFirstTask);

    // get the simple controller configurations for left and right motor drivers.
    // This gets the com ports and the device numbers
    cnt = GetPrivateProfileString(section, "lport", "", mLComPort, sizeof mLComPort, fname);
    journal("lport", mLComPort);
    mLDevice = (JournalEmit::Level)GetPrivateProfileInt(section, "ldevice", 10, fname);
    journal("ldevice", mLDevice);

    GetPrivateProfileString(section, "rport", "", mRComPort, sizeof mRComPort, fname);
    journal("rport", mRComPort);
    mRDevice = (JournalEmit::Level)GetPrivateProfileInt(section, "rdevice", 11, fname);
    journal("rdevice", mRDevice);

    // get the maestro controller configuration information
    GetPrivateProfileString(section, "mport", "", mMaestroComPort, sizeof mMaestroComPort, fname);
    journal("mport", mMaestroComPort);
    mMaestroDevice = (JournalEmit::Level)GetPrivateProfileInt(section, "mdevice", 12, fname);
    journal("mdevice", mMaestroDevice);

    GetPrivateProfileString(section, "ttlport", "", mTTLComPort, sizeof mTTLComPort, fname);
    journal("ttlport", mTTLComPort);

    // get inhibit drive motor flag - this is for testing without having the 'bot running around
    mInhibitDrive = GetPrivateProfileInt(section, "inhibit", 1, fname) == 1;
    journal("inhibit", mInhibitDrive);

    // get inhibit RC flag - this is so the judges cannot play with the 'bot
    mInhibitRC = GetPrivateProfileInt(section, "inhibit_rc", 1, fname) == 1;
    journal("inhibit RC", mInhibitRC);

    // get the rate for the processing loop
    mUpdateRate = GetPrivateProfileInt(section, "update_rate", 200, fname);
    journal("update_rate", mUpdateRate);

    // get the RoboRealm startup and API configuration information
    GetPrivateProfileString(section, "RoboRealmExe", "RoboRealm.exe", mRoboRealmExec, sizeof mRoboRealmExec, fname);
    journal("RoboRealmExe", mRoboRealmExec);

    mRoboRealmPort = GetPrivateProfileInt(section, "RoboRealmPort", 6060, fname);
    journal("RoboRealmPort", mRoboRealmPort);

    GetPrivateProfileString(section, "RoboRealmProg", "", mRoboRealmProg, sizeof mRoboRealmProg, fname);
    journal("mRoboRealmProg", mRoboRealmProg);

    // get any configuration data that is specific for this robot
    char* comp_name = getenv("COMPUTERNAME");
    journal("name", comp_name);

    cnt = GetPrivateProfileString(comp_name, "task", mFirstTask, mFirstTask, sizeof mFirstTask, fname);
    journal("task", mFirstTask);

    mInhibitDrive = GetPrivateProfileInt(comp_name, "inhibit", mInhibitDrive, fname) == 1;
    journal("inhibit", mInhibitDrive);

}
