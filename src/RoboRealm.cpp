#include "OS.h"

#include "ProjectStd.h"
#include "Journal.h"

#include "RoboRealm.h"
#include "RoboRealmVars.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
static Journal detail(JournalEmit::eDetail, __FILE__);
//---------------------------------------------------------------------------------------------------------------------
RoboRealm::RoboRealm(char* rr_exe, int rr_port, char* rr_prog) : 
    mRoboRealmExe(rr_exe), 
    mRoboRealmPort(rr_port), 
    mRoboRealmProg(rr_prog),
    mScript(),

    // careful on the ordering - make sure the *Vars are after the actual variable here and at their definitions
    mOrangeFence("orange_fence", mRR_API),
    mPosX("x_pos", mRR_API),
    mPosY("y_pos", mRR_API),
    mLabel("label", mRR_API),
    mDistance("distance", mRR_API),
    mConfidence("confidence", mRR_API),

    mGetVars(mRR_API),

    mLeftSpeed("left_speed", mRR_API),
    mRightSpeed("right_speed", mRR_API),
    mRetarget("retarget", mRR_API),
    mSetVars(mRR_API)
{
    // var read from RR
    mGetVars.add(mOrangeFence);

    mGetVars.add(mPosX);
    mGetVars.add(mPosY);

    mGetVars.add(mLabel); 
    mGetVars.add(mDistance);
    mGetVars.add(mConfidence);

    // vars written to RR
    mSetVars.add(mLeftSpeed);
    mSetVars.add(mRightSpeed);
    mSetVars.add(mRetarget);
}
//---------------------------------------------------------------------------------------------------------------------
RoboRealm::~RoboRealm()
{
    detail("Destructor");
    mRR_API.close();
    mRR_API.disconnect();
    detail("Destructor", "done");
}
//----------------------------------------------------------------------------------------------------------------------
void RoboRealm::executeScript()
{
    detail("RoboRealm script"/*, script*/);
    mRR_API.pause();
    mRR_API.execute(const_cast<char*>(mScript.c_str()));
    mRR_API.resume();
}
//----------------------------------------------------------------------------------------------------------------------
void RoboRealm::getData(RobotData& rd)
{
    detail("getData");
    rd.mIsOrangeFence = mOrangeFence == 1;
    rd.mPosX = mPosX;
    rd.mPosY = mPosY;
    rd.mName = mLabel;
    rd.mDistance = mDistance;
    rd.mConfidence = mConfidence;
    detail("getData done");
}
//---------------------------------------------------------------------------------------------------------------------
void RoboRealm::init()
{
    detail("Starting RoboRealm", mRoboRealmExe);
    if (!mRR_API.open(mRoboRealmExe, mRoboRealmPort))   // -faceless ???
    {
        journal("Could not start RoboRealm");
        throw runtime_error("Could not start RoboRealm");
    }

    detail("Connecting RoboRealm", mRoboRealmPort);
    if (!mRR_API.connect("localhost", mRoboRealmPort))
    {
        journal("Could not connect to RoboRealm");
        throw runtime_error("Could not connect to RoboRealm");
    }

    mRR_API.positionWindow(138, 0, 1100, 950);
    //mRR_API.minimizeWindow();
    //::Sleep(1000);

    trace("RoboRealm started");
}
//---------------------------------------------------------------------------------------------------------------------
int RoboRealm::read()
{
    detail("read");
    int cnt = mGetVars.get();

    detail("Orange", mOrangeFence);
    detail("Pos", "X", mPosX, "Y", mPosY);
    detail("get name", mLabel);
    detail("get fid", "Conf", mConfidence, "Dist", mDistance);
    return cnt;
}
//----------------------------------------------------------------------------------------------------------------------
// Called by RRThread to get data from RR
// 
void RoboRealm::setData(RobotControl& rc)
{
    mLeftSpeed = rc.mLeftSpeed;
    mRightSpeed = rc.mRightSpeed;
    mRetarget = rc.mRetarget;
}
//---------------------------------------------------------------------------------------------------------------------
// Called by RRThread to update data in RR
// 
void RoboRealm::write()
{
    detail("write");
    mSetVars.set();

    if (mScript.length() != 0) {
        executeScript();
        mScript.clear();
    }

    detail("Write", "L", mLeftSpeed, "R", mRightSpeed);
    detail( "Retarget", mRetarget);
    detail("write", "done");
}
