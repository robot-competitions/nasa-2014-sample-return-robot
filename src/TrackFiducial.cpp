//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;

#include "ProjectStd.h"
#include "Journal.h"

#include "Configuration.h"
#include "Behaviors.h"
#include "PIDControl.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
//  Track a fiducial using inputs from RoboRealm vision processing
void TrackFiducial::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) 
{
    detail("TrackFiducial");

    if (!mPreempted)
    {
        if (mName == rd.mName) {
            mPreempted = true;

            int pos_x = rd.mPosX;
            int abs_x = abs(pos_x);

            detail("PosX", pos_x);

            double rate = mPID.update(pos_x);
            mLastX = pos_x;

            stringstream s;
            s   << "X" << setw(5) << pos_x << " Rate" << setw(5) << (int)rate << " Kp"
                << setw(8) << mPID.proportionalGain()  << " Ki"
                << setw(8) << mPID.integralGain() << " I"
                << setw(5) << (int)mPID.integrator();

            detail(s.str());

            if (abs_x > 200) {
                mPID.clearIntegrator();

                rc.mRetarget = true;
                rc.mLeftSpeed = Configuration::mStandardSpeed;
                rc.mRightSpeed = Configuration::mStandardSpeed;
            }
            else {
                rc.mRetarget = false;
                rc.mLeftSpeed = Configuration::mStandardSpeed + (int)rate;
                rc.mRightSpeed = Configuration::mStandardSpeed - (int)rate;
            }
            detail("TrackFiducial", "L", rc.mLeftSpeed, "R", rc.mRightSpeed, "Retarget", rc.mRetarget);
        }
    }
}
