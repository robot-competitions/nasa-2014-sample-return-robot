//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <algorithm>
#include <ctime>
#include <functional>
#include <vector>
#include <fstream>
using namespace std;

#include "Behaviors.h"
#include "DoBehavior.h"
#include "Robot.h"
#include "Subsumption.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
extern Goal& StartupGoal;       // a reference to the startup goal. It is set in Goals
//----------------------------------------------------------------------------------------------------------------------
Subsumption::Subsumption(Robot& r) : mRobot(r), mCurGoal(&StartupGoal), mNewGoal(0)
{
    detail("Subsumption");
    BehaviorAbstract::setTasks(mCurGoal->tasks());
}
//----------------------------------------------------------------------------------------------------------------------
//  Get the robot data and setup a clean robot control structure
//  Run each behavior in the list of behaviors
//  If a new goal was requested then setup to use it on the next pass
//  
void Subsumption::operator()()
{
    clock_t ct = clock();

    detail("operator()");
    BehaviorAbstract::clearPreempted();

    RobotData rd = mRobot.getData();
    RobotControl rc;
    DoBehavior db(*this, rd, rc);

    TaskVector& tasks = mCurGoal->tasks();
    for_each(tasks.begin(), tasks.end(), bind1st(mem_fun(&DoBehavior::exec), &db));

    if (mNewGoal) {
        detail("subsumption new goal");
        mCurGoal = mNewGoal;
        mNewGoal = 0;

        BehaviorAbstract::setTasks(mCurGoal->tasks());
        reset();
    }

    
    mRobot.controlUpdate(rc);

    detail("operator()", clock() - ct);
}
//----------------------------------------------------------------------------------------------------------------------
//  Load a new pipeline and reset all the tasks from the new goal 
//  
void Subsumption::reset()   // move this to goal...
{
    detail("reset", mCurGoal->name());
    mRobot.updatePipeline(mCurGoal->pipeline());
    BehaviorAbstract::resetAll();
    detail("pipeline and tasks reset");
}
