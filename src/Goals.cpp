//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "Configuration.h"
#include "Behaviors.h"
#include "Subsumption.h"
#include "Goal.h"
#include "ButtonsImpl.h"
#include "Startup.h"

namespace gl {
//======================================================================================================================
// A couple of macros (shudder!!!) to help defining tasks - it would be nice to move this into Goal but time limitations...

#define TASK_BEGIN(NAME) TaskList NAME = { &led, &button_b, &button_a, &button_c, &button_d, &panic, &rc_control,

// put additional behaviors between these two macro invocations
#define TASK_END 0
#define GOAL_SWITCH(NEW_GOAL) new NewGoal(NEW_GOAL), 0
// follow the TASK_END / TASK_SWITCH with a };

//======================================================================================================================
//  Now a couple more macros (double shudder!!!) for the RR pipeline definition. Same usage and comment as above

#define RR_BEGIN(NAME) RRList NAME = { "header", "avi_setup", "orange_fence",

#define RR_END "display_std_vars", "mosaic", "avi_write", 0 
//======================================================================================================================
//  "Forward" declaration of goals so they can be used in other goals
extern Goal Carry;
extern Goal ClearPlatform;
extern Goal DropBack;
extern Goal DropForward;
extern Goal EnaDance;
extern Goal Idle;
extern Goal ScanForBase;
extern Goal Pickup;
extern Goal ScanForWhiteBldg;
extern Goal TrackBase;
extern Goal TestGoal1;
extern Goal TrackAnchor;
extern Goal TurnLeft;
extern Goal TurnRight;

extern Buttons<A> button_a;
extern Buttons<B> button_b;
extern Buttons<C> button_c;
extern Buttons<D> button_d;
//======================================================================================================================
// Behavior instances
//----------------------------------------------------------------------------------------------------------------------
//  These behaviors are put at the beginning of every TaskVector

LED led;
Panic panic;
RCControl rc_control;
Reset reset;
//----------------------------------------------------------------------------------------------------------------------
FindImage find_image;
Invert lose_image(&find_image);
//----------------------------------------------------------------------------------------------------------------------
//  Movements
//  redefine speed settings to shorten the typing when defining moves
static const int& highSpeed = Configuration::mHighSpeed;
static const int& standardSpeed = Configuration::mStandardSpeed;
static const int& midSpeed = Configuration::mMidSpeed;
static const int& slowSpeed = Configuration::mSlowSpeed;
static const int& crawlSpeed = Configuration::mCrawlSpeed;

Move cruise;

Move back_curve_right("BackCurveRight", midSpeed, -highSpeed, 500);
Move back_curve_left("BackCurveLeft", -highSpeed, midSpeed, 600);

Move curve_right("CurveRight", highSpeed, midSpeed, 500);
Move curve_left("CurveLeft", midSpeed, highSpeed, 500);

Move curve_right_10000_HS("CurveRight", highSpeed, standardSpeed, 10000);   // barely turn
Move curve_right_10000_HM("CurveRight", highSpeed, midSpeed, 10000);    // 14 foot, nearly 1/2 circle
Move curve_right_10000_HL("CurveRight", highSpeed, slowSpeed, 10000);   // 6foot 3/4 circle
Move curve_right_10000_HC("CurveRight", highSpeed, crawlSpeed, 10000);  // hardly move

Move curve_right_1000_HL("CurveRight", highSpeed, slowSpeed, 1000);
Move curve_left_1000_HL("CurveLeft", slowSpeed, highSpeed, 1000);

Move curve_right_3000_SM("CurveRight", standardSpeed, midSpeed, 10000);
Move curve_left_3000_SM("CurveLeft", midSpeed, standardSpeed, 10000);

Move curve_right_10000_SL("CurveRight", standardSpeed, slowSpeed, 10000);
Move curve_right_10000_SC("CurveRight", standardSpeed, slowSpeed, 10000);

Move pivot_right("PivotRight", -slowSpeed, -standardSpeed, 1000);
Move pivot_left("PivotLeft", standardSpeed, slowSpeed, 1000);

Move idle("Idle", 0, 0);
Move wait_2000("wait_2000", 0, 2000);
Move wait_4000("wait_4000", 0, 4000);

Cruise cruise_std;
                                                                //  hard    grass (mm)
Move forward_full_500("Forward500", highSpeed, 500);
Move forward_full_1000("Forward1000", highSpeed, 1000);         //  185     130

Move foward_std_500("FowardStd500", standardSpeed, 500);
Move foward_std_1000("FowardStd1000", standardSpeed, 1000);     //  80      55
Move foward_std_2000("FowardStd2000", standardSpeed, 2000);     //  80      55
Move foward_std_3000("FowardStd3000", standardSpeed, 3000);     //  80      55
Move foward_std_4000("FowardStd4000", standardSpeed, 4000);     
Move foward_std_50k("FowardStd50k", standardSpeed, 50000);     

Move foward_mid_500("FowardMid500", midSpeed, 500);
Move foward_mid_1000("ForwardMid1000", midSpeed, 1000);         //  60      40

Move foward_slow_500("FowardSlow500", slowSpeed, 500);
Move foward_slow_1000("ForwardSlow1000", slowSpeed, 1000);      //  40      25

Move foward_crawl_500("FowardCrawl500", crawlSpeed, 500);
Move foward_crawl_1000("ForwardCrawl1000", crawlSpeed, 1000);   //  ??      not useful

Move reverse_full500("RevFull500", highSpeed, 500);
Move reverse_full_1500("TwoBack", -standardSpeed, 1500);
Move reverse_slow_1000("ReverseSlow", -slowSpeed, 1000);
Move reverse_mid_1000("ReverseMid", -midSpeed, 1000);

Track track;
TrackFiducial fiducial_one("One");
//----------------------------------------------------------------------------------------------------------------------
//  Special actions on inputs

Move xcurve_right_8000_SM("CurveRight", highSpeed, midSpeed, 5000);
Move xcurve_left_8000_SM("CurveLeft", midSpeed, highSpeed, 5000);

OrangeFence orange_fence;
// three, two, one
Startup startup(xcurve_left_8000_SM, foward_std_3000, xcurve_right_8000_SM);
TurnToImage turn_to_image(curve_right_1000_HL, foward_std_3000, curve_left_1000_HL);
//----------------------------------------------------------------------------------------------------------------------
//  Servo positioning

ServoMove servo_ready("Ready", PAN_FORWARD, TILT_HORIZ, LIFT_UP, 0, false);
ServoMove servo_search("Search", PAN_FORWARD, TILT_SEARCH, LIFT_UP);
ServoMove servo_carry("Carry", PAN_FORWARD, TILT_SEARCH, LIFT_CARRY, 0);
ServoMove servo_pickup("Pickup", PAN_FORWARD, TILT_SEARCH, LIFT_DOWN);
ServoMove servo_drop("Drop", PAN_FORWARD, TILT_SEARCH, 1000, 0, true);
ServoMove servo_back("Back", PAN_BACK_RIGHT);

ServoPan servo_pan;
//======================================================================================================================
// DOCK - Goal to drive forward toward a slot fiducial on the home base and stop when it is a certain distance away.
// Presumably the area between the rover and the platform is clear. The camera is forward and horizontal to bring the 
// fiducial into full view.
// 

TASK_BEGIN(dock_tasks) &servo_ready/*, &track_slot*/, &idle, TASK_END };
RR_BEGIN(rr_dock) "name_dock", "dock", "fiducial_vars", "marker_for_vars",  RR_END};
Goal Dock("Dock", dock_tasks, rr_dock);
//======================================================================================================================
// DROP_BACK - Goal to drive backward and drop-off sample - drop lifter, drive back, raise lifter

TASK_BEGIN(drop_back) &servo_drop, &reverse_mid_1000, &servo_search, GOAL_SWITCH(Idle)};
RR_BEGIN(rr_drop_back) "name_drop_back", "marker_for_vars", RR_END };
Goal DropBack("Drop", drop_back, rr_drop_back);
//======================================================================================================================
// DROP_FORWARD - Goal to drive forward into position to drop sample - forward, drop, start DROP_BACK

TASK_BEGIN(drop_forward) &foward_mid_1000, &servo_drop, GOAL_SWITCH(DropBack)};
RR_BEGIN(rr_drop_forward) "name_drop_forward", "marker_for_vars", RR_END };
Goal DropForward("DropForward", drop_forward, rr_drop_forward);
//======================================================================================================================
//  Idle - do nothing
TASK_BEGIN(idle_task) &servo_ready, &idle, TASK_END };
RR_BEGIN(rr_idle) "name_idle", "marker_for_vars", RR_END };
Goal Idle("Idle", idle_task, rr_idle);
//======================================================================================================================
// PICKUP - Goal to drive forward and pick-up sample - drop lifter, drive forward, raise lifter

TASK_BEGIN(pickup_task) &servo_pickup, &forward_full_1000, GOAL_SWITCH(Carry)};
RR_BEGIN(rr_pickup) "name_pickup", "marker_for_vars", RR_END };
Goal Pickup("Pickup", pickup_task, rr_pickup);
//======================================================================================================================
// CARRY - Goal to carry sample

TASK_BEGIN(carry_task) &servo_carry, &foward_std_2000, GOAL_SWITCH(Idle)};
RR_BEGIN(rr_carry_task) "name_pickup", "marker_for_vars", RR_END };
Goal Carry("Carry", carry_task, rr_carry_task);
//======================================================================================================================
// TrackAnchor - Drive forward tracking straight ahead using VisualAnchor
// 
TASK_BEGIN(track_tasks) &servo_search, &track, TASK_END };
RR_BEGIN(rr_track) "visual_anchor", "marker_for_vars", RR_END };
Goal TrackAnchor("TrackAnchor", track_tasks, rr_track);
//======================================================================================================================
// TrackBase - Drive toward the base tracking one of the two base fiducials
// 
TASK_BEGIN(track_base) &servo_ready, &fiducial_one, TASK_END };
RR_BEGIN(rr_track_base) "track_base", "fiducial_vars", RR_END };
Goal TrackBase("TrackBase", track_base, rr_track_base);


//======================================================================================================================
// TestGoal - Goal for messing around with other goals defined on the buttons
// 
TASK_BEGIN(test_tasks1) &servo_pan, &servo_pickup, &wait_4000, GOAL_SWITCH(TestGoal1) };
TASK_BEGIN(test_tasks2) &servo_ready, &curve_left_1000_HL, &idle, GOAL_SWITCH(Idle) };
TASK_BEGIN(test_tasks3) &servo_ready, &curve_right_1000_HL, &idle, GOAL_SWITCH(Idle) };
TASK_BEGIN(test_tasks4) &servo_ready, &curve_right_10000_HC, &idle, GOAL_SWITCH(Idle) };

RR_BEGIN(rr_test) "name_test", "marker_src", "marker_for_vars",  RR_END };
Goal TestGoal1("TestGoal", test_tasks1, rr_test);
Goal TestGoal2("TestGoal", test_tasks2, rr_test);
Goal TestGoal3("TestGoal", test_tasks3, rr_test);
Goal TestGoal4("TestGoal", test_tasks4, rr_test);
//======================================================================================================================
//      ENA DANCE - &pivot_left, &pivot_right, &reset,
//      
TASK_BEGIN(ena_dance) &pivot_left, &pivot_right, GOAL_SWITCH(EnaDance) };
Goal EnaDance("Ena Dance", ena_dance, rr_test);

//----------------------------------------------------------------------------------------------------------------------
//  Button actions
extern Goal ApproachBase;
NewGoal a_goal(Idle);
NewGoal b_goal(Pickup);
NewGoal c_goal(DropForward);
NewGoal d_goal(TrackAnchor);

Buttons<A> button_a(a_goal);
Buttons<B> button_b(b_goal);
Buttons<C> button_c(c_goal);
Buttons<D> button_d(d_goal);
//======================================================================================================================
//  ApproachBase - Goal to move rover from where it loses signage to climb home base
NewGoal ng(Idle);
FindImage fi(&ng);

//TASK_BEGIN(approach_base_task) &fi, &cruise_std, &servo_ready, TASK_END};
//RR_BEGIN(rr_scan_base) "find_homebase", "marker_for_vars", RR_END };
//Goal ApproachBase("ApproachBase", approach_base_task, rr_scan_base);

//======================================================================================================================
// Charge - Goal to travel to near the sample

TASK_BEGIN(charge_task) &servo_ready, &foward_std_50k, GOAL_SWITCH(TurnRight)};
RR_BEGIN(rr_charge) "name_charge", "marker_for_vars", RR_END };
Goal Charge("Charge", charge_task, rr_charge);

//======================================================================================================================
// TurnRight

TASK_BEGIN(turn_r_task) &servo_ready, &xcurve_right_8000_SM, GOAL_SWITCH(Idle)};
Goal TurnRight("TurnRight", turn_r_task, rr_charge);

//======================================================================================================================
// TurnLeft - 

TASK_BEGIN(turn_l_task) &servo_ready, &xcurve_left_8000_SM, GOAL_SWITCH(Idle)};
Goal TurnLeft("TurnLeft", turn_l_task, rr_charge);

//======================================================================================================================
// ClearPlatform - Goal to drive off platform with servos in neutral position

TASK_BEGIN(clear_platform_task) &servo_ready, &startup, GOAL_SWITCH(Charge)};
RR_BEGIN(rr_clear_platform) "name_clear", "marker_for_vars", RR_END };
Goal ClearPlatform("Clear Platform", clear_platform_task, rr_clear_platform);

//======================================================================================================================
//  ScanForWhiteBldg - Goal to determine if rover can see the white building and its bearing from the rover

TASK_BEGIN(scan_wb_task) &turn_to_image, &find_image, &servo_pan, GOAL_SWITCH(Idle)};
RR_BEGIN(rr_scan_wb) "find_white_bldg", "marker_for_vars", RR_END };
Goal ScanForWhiteBldg("ScanForWhiteBldg", scan_wb_task, rr_scan_wb);
//======================================================================================================================
//  ScanForBase - Goal to determine if rover can see home base and its bearing from the rover

TASK_BEGIN(scan_base_task) &turn_to_image, &find_image, &servo_pan, GOAL_SWITCH(ScanForBase)};
RR_BEGIN(rr_scan_base) "find_homebase", "marker_for_vars", RR_END };
Goal ScanForBase("ScanForBase", scan_base_task, rr_scan_base);

} // namespace gl

//======================================================================================================================
//  StartupGoal is a reference to the goal that Subsumption should use at startup
Goal& StartupGoal = gl::TrackAnchor;
