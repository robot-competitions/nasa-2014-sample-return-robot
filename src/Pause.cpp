//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <fstream>
#include <string>
#include <time.h>
using namespace std;

#include "Journal.h"
#include "Pause.h"

bool Pause::mIsPaused = false;
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
struct PauseData {
    char* mComp;
    int mRetries;
};

PauseData comps[] = {
    { "\\\\mystic-three\\mystic\\pause",4 }, 
    { "\\\\mystic-two\\mystic\\pause", 4 },
    { "\\\\mystic-one\\mystic\\pause", 4 },
};

char* p = "d:\\mystic\\pause";
//----------------------------------------------------------------------------------------------------------------------
void Pause::anyPause()
{
    detail("anyPause");
    for (int i = 0; i < 3; i++) { 
        int& r = comps[i].mRetries;

        if (r < 0) {

            // must be offline or out of reach so do not retry for awhile
            r--;
            if (r < -20) {
                r = 5;      // start checking again
            }
            break;
        }
        clock_t cur = clock();

        detail(comps[i].mComp, r);
        ifstream in(comps[i].mComp);

        if (in.good()) {
            detail("anyPause", "pause");
            mIsPaused = true;
            return;
        }
        else if (cur + 1000 < clock()) {    // took to long to was a timeout
            r--;
        }
    }
    mIsPaused = false;
}
//----------------------------------------------------------------------------------------------------------------------
void Pause::set()
{
    ofstream of(p);
    of.close();
}
//----------------------------------------------------------------------------------------------------------------------
void Pause::clear()
{
    remove(p);
}
