#include "ProjectStd.h"
#include "Journal.h"

#include "Configuration.h"
#include "Behaviors.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
bool Flee::operator()(const bool preempted) 
{ 
    if (!preempted)
    {
        if (BehaviorAbstract::mFlight.mActive)
        {
            trace("active");
            mFlight = BehaviorAbstract::mFlight;
            BehaviorAbstract::mFlight.mActive = false;

            mState = init;
            mWasActive = stateMachine();
        }
        else if (mWasActive)
        {
            mWasActive = stateMachine();
        }
        else
        {
            mWasActive = false;
        }
    }
    return mWasActive; 
}
//----------------------------------------------------------------------------------------------------------------------
bool Flee::stateMachine()
{
    bool stayactive = true;
    const int flee_speed = standard_speed;

    switch (mState)
    {
    case init:
        journal("init");
        mClockTimeout = time_for_distance(flee_speed, mFlight.mBackup) + mClockTime + behavior_thread_time;
        trace("backing until ", mClockTimeout);
        mState = backing;

    case backing:
//            mCreate->directDrive(-flee_speed, -flee_speed);

            if (mClockTimeout < mClockTime)
            {
                mState = spining;
                mClockTimeout = time_for_angle(flee_speed, mFlight.mSpin) + mClockTime + behavior_thread_time;
                trace("spinning until ", mClockTimeout);
            }
            else
            {
                break;
            }

    case spining:
        {
            int speed = (mFlight.mLeft ? flee_speed : -flee_speed);
//            mCreate->directDrive(speed, -speed);

            if (mClockTimeout < mClockTime)
            {
//                mCreate->directDrive(0, 0);
                mState = driving;
                mClockTimeout = time_for_distance(flee_speed, mFlight.mDrive) + mClockTime + behavior_thread_time;
                trace("driving until ", mClockTimeout);
            }
            else
            {
                break;
            }
        }

    case driving:
//            mCreate->directDrive(flee_speed, flee_speed);

            if (mClockTimeout < mClockTime)
            {
                mFlight.mActive = false;
                stayactive = false;
            }
            else
            {
                journal("done");
            }
            break;
    }
    return stayactive;
}
