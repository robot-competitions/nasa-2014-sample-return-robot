//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <fstream>
#include "ProjectStd.h"
using namespace std;

#include "OS.h"
#include "Thread.h"
#include "ThreadTimer.h"
#include "Journal.h"

#include "Configuration.h"
#include "SRR.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
SRR::SRR() :

    mRRWatchDog(0), mBWatchDog(0), mPauseDog(0),

    mBehaviorThread(mBWatchDog), mRRThread(mBehaviorThread.roboRealm(), mRRWatchDog)
//mPauseThread(mPauseDog)
{
    Thread::stopDelay(Configuration::mUpdateRate);
}
//---------------------------------------------------------------------------------------------------------------------
SRR::~SRR() {
    detail("SRR destructor called");
    Thread::stop();
    detail("SRR destructor exiting");
}
//---------------------------------------------------------------------------------------------------------------------
void SRR::operator()() {
    ::sleep(Configuration::mUpdateRate);

    mRRThread();
    //mPauseThread();
    mBehaviorThread();

    ThreadTimer tt(Configuration::mUpdateRate);

    HANDLE stop_event = Thread::stopEvent();
    while (WaitForSingleObject(stop_event, 0) != WAIT_OBJECT_0) {
        ::Sleep(Configuration::mUpdateRate);
        detail("Checking for break and watchdog");

        if (fileCheck("break")) break;
        watchdogCheck();
        tt.wait();
    }
}
//----------------------------------------------------------------------------------------------------------------------
bool SRR::fileCheck(c_char* fname) {
    fstream f;
    f.open(fname, ios_base::in);
    if (f.is_open()) {
        journal("Detected", fname);
        return true;
    }
    return false;
}
//----------------------------------------------------------------------------------------------------------------------
void SRR::watchdogCheck() {
    watchdog(mRRWatchDog, Configuration::mWatchDogLimit * 4, "RoboRealm");
    watchdog(mBWatchDog, Configuration::mWatchDogLimit, "Behavior");
}
//----------------------------------------------------------------------------------------------------------------------
void SRR::watchdog(int& watchdog, int limit, string text) {
    detail(text, watchdog);
    if (watchdog++ > limit) {
        journal(text + " Watchdog Expired");
        throw runtime_error(text + " Watchdog Expired");
    }
}
