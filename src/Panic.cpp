//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "ProjectStd.h"
#include "Journal.h"
#include "Configuration.h"
#include "Behaviors.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
void Panic::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) 
{ 
    detail("Panic");

    if (!mPreempted) {
        // see if the Thumper controllers are faulted, i.e. safe start violation on which prevents motor movement
        if (rd.mLeftDrive.mIsFaulted || rd.mRightDrive.mIsFaulted) {

            detail("Panic", "Faulted");

            if (rd.mLeftDrive.mIsLowVin || rd.mRightDrive.mIsLowVin) {

                mWasActive = true;  // lets stop doing things

                // if vin low for less than 1.5 seconds try reseting
                if (mLowVinCnt++ < (1500 / Configuration::mUpdateRate)) {
                    journal("PANIC Low Vin Reset **************", mLowVinCnt);
                    rc.mSafeReset = true;
                }
                else {
                    journal("Low Vin Hold", mLowVinCnt);
                }
            }
            else {
                journal("PANIC reset safe start");
                rc.mSafeReset = true;
                mLowVinCnt = 0;
            }
        }
    }
}
