//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <functional>
#include <vector>
using namespace std;

#include "ProjectStd.h"
#include "Thread.h"
#include "ThreadTimer.h"
#include "Journal.h"
#include "Robot.h"
#include "BehaviorAbstract.h"
#include "Subsumption.h"

#include "BehaviorThread.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
BehaviorThread::BehaviorThread(int& wd) : 
    Thread(wd),     
    mRoboRealm(Configuration::mRoboRealmExec, Configuration::mRoboRealmPort, Configuration::mRoboRealmProg),
    mRobot(mRoboRealm), 
    mSubsumption(mRobot)
{
}
//----------------------------------------------------------------------------------------------------------------------
void BehaviorThread::run()
{
    trace("Behavior Thread running");
    ThreadTimer tt(Configuration::mUpdateRate);

//    ::Sleep(Configuration::mUpdateRate);

    mSubsumption.reset();   // reset the startup goal behaviors

    // keep running until the stop event is signaled
    while (Thread::isStopping())
    {
        mSubsumption();
        tt.wait();
    }
    trace("Behavior Thread stopping");
}
