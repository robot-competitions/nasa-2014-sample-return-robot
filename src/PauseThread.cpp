//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "ProjectStd.h"
#include "Thread.h"
#include "ThreadTimer.h"
#include "Journal.h"

#include "PauseThread.h"
#include "Configuration.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
PauseThread::PauseThread(int& wd)  : 
    Thread(wd)
{
}
//----------------------------------------------------------------------------------------------------------------------
void PauseThread::run()
{
    trace("Pause Thread running");
    ThreadTimer tt(Configuration::mUpdateRate);

    ::Sleep(Configuration::mUpdateRate);
    // keep running until the stop event is signaled
    while (isStopping())
    {
        //Pause::anyPause();
        tt.wait();
    }
    trace("Pause Thread stopping");

}