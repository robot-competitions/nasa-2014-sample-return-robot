//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <fstream>
#include <iostream>

#include "BehaviorAbstract.h"
#include "Goal.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
Goal::Goal(char* n, TaskList tl, RRList rrl) : mName(n)
{
    detail(mName);

    Behavior* list_end = tl;
    while (*list_end++); 
    list_end--;
    mTasks = TaskVector(tl, list_end);

    buildPipeline(rrl);
}
//----------------------------------------------------------------------------------------------------------------------
void Goal::buildPipeline(RRList fnames) 
{
    detail("buildPipeline");

    for (int i = 0; fnames[i] != 0; i++) {
        string text;
        string fname(fnames[i]);

        fname = "robo/" + fname + ".robo";

        ifstream in(fname.c_str());

        getline(in, text, char(0));
        mPipeline += text;

        detail(fname);
        detail(text);
    }
    detail("buildPipeline done");
}
