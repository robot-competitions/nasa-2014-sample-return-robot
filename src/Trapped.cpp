#include "Behaviors.h"
//#include "Create.h"

unsigned int const wait_time = 10000;
//----------------------------------------------------------------------------------------------------------------------
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
Trapped::Trapped() :
    mIsBusy(false), mLastLocation(create.traveledDistance()), mTimeout(Task::ticks() + wait_time * 3) {
}
//----------------------------------------------------------------------------------------------------------------------
bool Trapped::operator()(bool const preempted) {
    if (preempted) {
        reset();
    }
    else if (mIsBusy) {
        mIsBusy = stateMachine();
        if ( !mIsBusy) {
            reset();
        }
    }
    else {
        int dist = (mLastLocation - create.traveledDistance());

        // see if not moved much:	-standard_backup*2 < dist < standard_backup*2
        // within timeout period
        if (( -standard_backup * 2 < dist) && (dist < standard_backup * 2)) {
            // no movement - see how long
            if (mTimeout - Task::ticks() > 0x7FFF) {
                CbGrn2.set();

                mLocation = create.traveledDistance() - standard_backup;
                mAngle = create.traveledAngle() - (Task::ticks() % 180);

                mState = backing;
                mLeftBump = true;

                mWasActive = true;
                mIsBusy = true;
            }
        }
        else	// have moved so reset state
        {
            reset();
        }
    }

    return mWasActive;
}
//----------------------------------------------------------------------------------------------------------------------
void Trapped::reset() {
    CbGrn2.reset();
    mLastLocation = create.traveledDistance();
    mTimeout = Task::ticks() + wait_time;
    mIsBusy = false;
    mWasActive = false;
}
