//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <ctime>

#include "ProjectStd.h"
#include "Journal.h"

#include "Configuration.h"
#include "Behaviors.h"
#include "Timer.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
// LED controls the flashing of the Safety LED. It is an unusual behavior since it runs all the time and cannot be 
// preempted. The LED flashes at 1hz when the robot is operational and is on when paused. 
// 
void LED::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) 
{
    if (rd.mIsPaused) {
        mLedState = true;
    }
    else if (isExpired()) {
        timerStart();
        mLedState = !mLedState;
    }
    rc.mSafetyLED = mLedState;
    detail("LED", rc.mSafetyLED);
}
