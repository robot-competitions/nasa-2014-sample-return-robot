//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
// 
#include <algorithm>
#include <functional>

#include "BehaviorAbstract.h"
//----------------------------------------------------------------------------------------------------------------------
TaskVector* BehaviorAbstract::mCurTasks;

bool BehaviorAbstract::mPreempted = false;

BehaviorAbstract::eLocation BehaviorAbstract::mLocation = start;
BehaviorAbstract::eOrientation BehaviorAbstract::mOrientation = unknown;
BehaviorAbstract::eSeeing BehaviorAbstract::mSeeing = nothing;
BehaviorAbstract::eWhere BehaviorAbstract::mWhere = front;
BehaviorAbstract::eStatus BehaviorAbstract::mStatus  = starting;

int BehaviorAbstract::mAngle = 180;
int BehaviorAbstract::mRcPos = 1500;
int BehaviorAbstract::mPosX = -1;
//----------------------------------------------------------------------------------------------------------------------
void BehaviorAbstract::exec_reset_behaviors(Behavior b)
{
    b->reset();
}
//----------------------------------------------------------------------------------------------------------------------
void BehaviorAbstract::resetAll()
{
    TaskVector& tasks = *mCurTasks;
    for_each(tasks.begin(), tasks.end(), &BehaviorAbstract::exec_reset_behaviors);
}
//----------------------------------------------------------------------------------------------------------------------
inline
const int BehaviorAbstract::msec_to_deg(const int msec)
{
    int ms = msec - 900;
    if (ms < 0) ms = 0;
    else if (ms > 1500) ms = 1500;
    return (ms * 360) / rc_range;
}
//----------------------------------------------------------------------------------------------------------------------
const void BehaviorAbstract::image_to_angle(const int px, const int msecs)
{
    mAngle = (camera_field_of_view / 2 - px_to_deg(mPosX)) + msec_to_deg(mRcPos);
}
//----------------------------------------------------------------------------------------------------------------------
const void BehaviorAbstract::which_direction()
{
    static const int limit = 15;
    mWhere = (mAngle > (180+limit) ? left : (mAngle < (180-limit) ? right : front));
}
