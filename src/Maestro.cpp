//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//
#include <string>
#include <utility>
using namespace std;

#include "Maestro.h"
//---------------------------------------------------------------------------------------------------------------------
inline
float Maestro::getAnalogIn(const unsigned char  channel /*= 0*/) const
{
    if (channel > MAX_ANALOG_CHANNEL) {
        char buf[10];
        _itoa_s(channel, buf, sizeof(buf), 10);
        throw invalid_argument(string("Attempt to read Maestro digital channel as analog") + string(buf));
    }
    return getQuarterMilliseconds(channel) * MAX_ANALOG_VOLTAGE / static_cast<float>(MAX_ANALOG_VALUE);
}
//---------------------------------------------------------------------------------------------------------------------
size_t Maestro::getErrors() const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x21, 0xFF};

    sendMsg(msg, sizeof(msg));
    mSerialPort.waitRxReady();

    unsigned char  buf[2];
    mSerialPort.read(buf, 2);

    size_t errors = (buf[1] << 8) | buf[0];
    return errors;
}
//---------------------------------------------------------------------------------------------------------------------
size_t Maestro::getQuarterMilliseconds(const unsigned char  channel /*= 0*/) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x10, channel, 0xFF};

    sendMsg(msg, sizeof(msg));
    mSerialPort.waitRxReady();

    unsigned char  buf[2];
    mSerialPort.read(buf, 2);

    size_t value = (buf[1] << 8) | buf[0];
    return value;
}
//---------------------------------------------------------------------------------------------------------------------
void Maestro::goHome() const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x22, 0xFF};

    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
bool Maestro::isMoving() const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x13, 0xFF};

    sendMsg(msg, sizeof(msg));
    mSerialPort.waitRxReady();
    return mSerialPort.read() == 1;
}
//---------------------------------------------------------------------------------------------------------------------
void Maestro::setAcceleration(const size_t accel, const unsigned char  channel /*= 0*/) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x09, channel, 0, 0, 0xFF};

    value_to_msg(&msg[4], accel);

    sendMsg(msg, sizeof(msg));
}

//---------------------------------------------------------------------------------------------------------------------
void Maestro::setQuarterMilliseconds(const size_t value, const unsigned char  channel /*= 0*/) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x04, channel, 0, 0, 0xFF};

    value_to_msg(&msg[4], value);

    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
void Maestro::setSpeed(const size_t speed, const unsigned char  channel /*= 0*/) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x07, channel, 0, 0, 0xFF};

    value_to_msg(&msg[4], speed);

    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
// times in 1/48 usec
//
void Maestro::setPWM(const size_t high_time, const size_t period) const
{
    unsigned char  msg[] = {START_unsigned char , mDevice, 0x0A, 0, 0, 0, 0, 0xFF};

    value_to_msg(&msg[3], high_time);
    value_to_msg(&msg[5], period);

    sendMsg(msg, sizeof(msg));
}
//---------------------------------------------------------------------------------------------------------------------
void Maestro::setMultipleTargets(const unsigned char  count, const size_t targets[], const unsigned char  first_channel /*= 0*/)
{
    unsigned char  msg[5 + 48 + 1] = {START_unsigned char , mDevice, 0x1F, count, first_channel};

    if (count > 24) {
        return;
    }

    for (int pos = 0; pos < count; pos++)
    {
        value_to_msg(&msg[5+(pos*2)], targets[pos]*4);
    }

    sendMsg(msg, (count*2) + 5 + 1);
}
