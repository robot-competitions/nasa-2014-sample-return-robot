//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <iomanip>
#include <sstream>
using namespace std;

#include "Journal.h"
#include "Configuration.h"
#include "Robot.h"
#include "Pause.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//======================================================================================================================
//  Maestro Controller Channels and Values
//====================================================================================================================== 
// Output Channel Assignments
// 
const unsigned char  LED = 0;
const unsigned char  DEAD = 1;
const unsigned char  pan = 2;
const unsigned char  tilt = 3;
const unsigned char  lift = 4;
const unsigned char  other = 10;

const size_t MAX_SERVO = 2100;
const size_t MIN_SERVO = 800;
//----------------------------------------------------------------------------------------------------------------------
// Input Channel Assignments
// 
const unsigned char  b_button = 14;
const unsigned char  d_button = 15;
const unsigned char  c_button = 16;
const unsigned char  a_button = 17;
//----------------------------------------------------------------------------------------------------------------------
Robot::Robot(RoboRealm& rr) : 
    mInhibitDrive(Configuration::mInhibitDrive),

    mLSerial(Configuration::mLComPort, CBR_115200),
    mRSerial(Configuration::mRComPort, CBR_115200),
    mMSerial(Configuration::mMaestroComPort, CBR_115200),

    mLeftDrive(Configuration::mLDevice, mLSerial),
    mRightDrive(Configuration::mRDevice, mRSerial),
    mMaestro(Configuration::mMaestroDevice, mMSerial),

    mLSimpleData(mRobotData.mLeftDrive),
    mRSimpleData(mRobotData.mRightDrive),
    mRcData(mRobotData.mRcData),

    mRoboRealm(rr)
{
    detail("Constructor");
    mRobotData.mIsPaused = false;
}
//----------------------------------------------------------------------------------------------------------------------
Robot::~Robot()
{
    detail("Destructor");
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::acquireData()
{
    detail("acquireData");

    acquireRRData();
    acquireMaestroData();
    acquireSimpleMCData(mLeftDrive, mLSimpleData, "left");
    acquireSimpleMCData(mRightDrive, mRSimpleData, "right");

    calculateRcData();
    detail("getData", "done");
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::acquireMaestroData()
{
    detail("acquireMaestroData");

    mRobotData.mButtons[A] = mMaestro.getDigitalInput(a_button);
    mRobotData.mButtons[B] = mMaestro.getDigitalInput(b_button);
    mRobotData.mButtons[C] = mMaestro.getDigitalInput(c_button);
    mRobotData.mButtons[D] = mMaestro.getDigitalInput(d_button);

    detail("Buttons", mRobotData.mButtons[A], mRobotData.mButtons[B], mRobotData.mButtons[C], mRobotData.mButtons[D]);

    mRobotData.mIsMoving = mMaestro.isMoving();
    mRobotData.mPanPos = mMaestro.getMilliseconds(pan);
    mRobotData.mTiltPos = mMaestro.getMilliseconds(tilt);
    mRobotData.mLiftPos = mMaestro.getMilliseconds(lift);

    detail("Moving", mRobotData.mIsMoving);
    detail("Pan", mRobotData.mPanPos);
}
//----------------------------------------------------------------------------------------------------------------------
// acquire data from RoboRealm
void Robot::acquireRRData()
{
    mRoboRealm.getData(mRobotData);
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::acquireSimpleMCData(PololuSimple& ps, SimpleMCData& s_data, char* side)
{
    detail("acquireSimpleData", side);
    s_data.limit = ps.getLimitStatus();     // actual read
    s_data.mIsFaulted = ps.isSafeErr();
    s_data.mIsLowVin = ps.isLowVin();

    s_data.err_occ = ps.getErrorOccurred(); // actual read
    s_data.low_vin = ps.isLowVin();
    s_data.is_kill = ps.isKillSwitch();

    s_data.analog1 = ps.getAnalog1Raw();
    s_data.temp = ps.getTemp() / 10;
    s_data.volts = ps.getVin() / 1000.0f;
    s_data.rc1_in = ps.getRC1Scaled();
    s_data.rc2_in = ps.getRC2Scaled();
    s_data.targ = ps.getTarget();

    static stringstream s;
    s.str("Status ");
    s   << " kill" << setw(5) << s_data.is_kill
        << " loV " << setw(5) << s_data.low_vin 
        << " err " << setw(5) << hex << s_data.err_occ  << dec
        << " lim " << setw(5) << hex << s_data.limit
        << dec;
    detail(s.str());

    s.str("");
    s.clear();  // some say this is needed 

    s.str("Data ");
    s   << "volts " << setw(5) << s_data.volts 
        << " amps " << setw(5) << s_data.analog1 
        << " temp " << setw(5) << s_data.temp 
        << " targ " << setw(5) << s_data.targ 
        << " rc1 " << setw(5) << s_data.rc1_in
        << " rc2 " << setw(5) << s_data.rc2_in;
    detail(s.str());

    detail("acquireSimpleData", "done");
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::calculateRcData()
{
    mRcData.mRc1 = (mRSimpleData.rc1_in * 100) / 3200;
    mRcData.mRc2 = (mRSimpleData.rc2_in * 100) / 3200;
    mRcData.mRc3 = mLeftDrive.getRC1Raw();

    detail("Raw RC3", mRcData.mRc3);
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::controlUpdate(const RobotControl& c)
{
    detail("updateControl");
    mRobotControl = c; 

    controlPause();     // keep this first so pause overrules everything else
    controlSpeed();
    controlMaestro();
    mRoboRealm.setData(mRobotControl);
}
//----------------------------------------------------------------------------------------------------------------------
const size_t LED_ON = 1000;
const size_t LED_OFF = 2000;
//----------------------------------------------------------------------------------------------------------------------
void Robot::controlMaestro()
{
    detail("controlMaestro");

    if (mRobotData.mIsPaused) {
        detail("controlMaestro", "paused");
        size_t targets[] = { 
            LED_ON, 
            1500, // dead channel on Mystic Two
            mRobotData.mPanPos, 
            mRobotData.mTiltPos,
            mRobotData.mLiftPos,
        };
        mMaestro.setMultipleTargets(5, targets, LED);
        detail("Safety", LED_ON);
        detail("Servo", "Pan", mRobotData.mPanPos, "Tilt", mRobotData.mTiltPos, "Lift", mRobotData.mLiftPos);
    }
    else {
        size_t targets[] = { 
            mRobotControl.mSafetyLED ? LED_ON : LED_OFF, 
            1500, // dead channel on Mystic Two
            mRobotControl.mPan, mRobotControl.mTilt, mRobotControl.mLift 
        };

        for (int i = 0; i < 5; i++) {
            targets[i] = targets[i] > MAX_SERVO ? MAX_SERVO : targets[i];
            targets[i] = targets[i] < MIN_SERVO ? MIN_SERVO : targets[i];
        }

        mMaestro.setMultipleTargets(5, targets, LED);
        detail("Safety", mRobotControl.mSafetyLED);
        detail("Servo", "Pan", mRobotControl.mPan, "Tilt", mRobotControl.mTilt, "Lift", mRobotControl.mLift);
    }
}
//----------------------------------------------------------------------------------------------------------------------
//  mActivate and mDeactive are set by behaviors
//  
void Robot::controlPause()
{
    if (mRobotControl.mActivatePause) {
        mRobotData.mIsPaused = true;
        Pause::set();
    }
    else if (mRobotControl.mDeactivatePause) {
        mRobotData.mIsPaused = false;
        Pause::clear();
    }

    mRobotData.mIsPaused = Pause::isPaused() || mRobotData.mIsPaused;
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::controlSpeed()
{
    if (mRobotControl.mSafeReset) {
        detail("Safety Reset"); 
        mLeftDrive.exitSafeStart();
        mRightDrive.exitSafeStart();
    }

    int& left = mRobotControl.mLeftSpeed;
    int& right = mRobotControl.mRightSpeed;

    detail("controlSpeed"); 
    detail("Input", "L", left, "R", right);

    // make sure values are within limits the controller accepts...
    // ...or the controller goes wonky
    limitSpeed(left);
    limitSpeed(right);
    detail("Limited", "L", left, "R", right);

    if (mInhibitDrive) {
        detail("Drive inhibited");
    }
    else if (mRobotData.mLeftDrive.volts > 9.0)  { // on high voltage power supply?
        emitSpeed(mLeftDrive, left * 3 / 4);
        emitSpeed(mRightDrive, right * 3 / 4);
    }
    else {
        emitSpeed(mLeftDrive, left);
        emitSpeed(mRightDrive, right);
    }

    detail("controlSpeed", "done");
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::emitSpeed(PololuSimple& ps, const int s)
{
    //if (mRobotData.mIsPaused) {
    //    detail("emitSpeed", "paused");
    //    ps.forwardPercent(0);
    //}
    //else {
    //    s > 0 ? ps.forwardPercent(s) : ps.reversePercent(-s);
    //}
    s > 0 ? ps.forwardPercent(s) : ps.reversePercent(-s);
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::limitSpeed(int& v, int controller_limit) 
{
    controller_limit = abs(controller_limit);
    if (v > 0 && v > controller_limit) v = controller_limit;
    else if (v < 0 && v < -controller_limit) v = -controller_limit;
}
//----------------------------------------------------------------------------------------------------------------------
void Robot::updatePipeline(string& pipeline)
{
    mRoboRealm.setScript(pipeline);
}
