//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;

#include "ProjectStd.h"
#include "Journal.h"

#include "Configuration.h"

#include "Robot.h"
#include "Behaviors.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
//  Checks if RC control is active (mRc3 postivie) and 
//  converts RC input of forward / reverse and left / right into differential speed
void RCControl::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) 
{
    static 	bool mWasActive;

    detail("RCControl");
    if (!mPreempted)
    {
        RcData rc_data = rd.mRcData;

        detail("RC", "mRc1", rc_data.mRc1, "mRc2", rc_data.mRc2);
        detail("RC3", rc_data.mRc3);

        if (rc_data.mRc3 > 5000) {  // Autonomous runnning - no RC control
            if (mWasActive) {
                //  The robot was under RC control so reset the current behaviors to start over. 
                resetAll();
            }
            mWasActive = false;
            rc.mDeactivatePause = true;
        }
        else {
            rc.mActivatePause = true;
            mWasActive = true;
            mPreempted = true;
            rc.mRetarget = true;

            if (rc_data.mRc3 < 4000) {
                // no RC input if it is less than 4000 (around 3891)
                detail("RCControl", "================== NO RC Signal ========================");
                rc.mLeftSpeed = 0;
                rc.mRightSpeed = 0;
            }
            else if (rc_data.mRc3 < 5000 && !Configuration::mInhibitRC) {  // input < 0 so exercising RC control
                // RC Control
                detail("RCControl", "================= RC Control ========================");

                int left = rc_data.mRc1 + rc_data.mRc2;
                int right = rc_data.mRc1 - rc_data.mRc2;

                left = int(left * 0.8);
                right = int(right * 0.8);

                rc.mLeftSpeed = left;
                rc.mRightSpeed = right;
                detail("RC Output", "L", left, "R", right);
            }
        }
    }
    detail("RCControl", "done");
}
