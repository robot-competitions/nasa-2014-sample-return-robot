//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "ProjectStd.h"
#include "Journal.h"

#include "Behaviors.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
#include "Startup.h"

void Startup::setRobotGoal(const string& c_name, RobotData& rd,
		RobotControl& rc, Subsumption& sub) {
	if (c_name == "MYSTIC-ONE") {
		(mLeft)(rd, rc, sub);
	} else if (c_name == "MYSTIC-TWO") {
		(mForward)(rd, rc, sub);
	} else {
		(mRight)(rd, rc, sub);
	}

	if (!mPreempted) {
		setActive();
	}
}

//----------------------------------------------------------------------------------------------------------------------
void Startup::operator()(RobotData& rd, RobotControl& rc, Subsumption& sub)
{
    detail(mName);

    if (!isActive()) {
        detail(mName, "startup");

        char* comp_name = getenv("COMPUTERNAME");
        detail(mName, comp_name);

        string c_name(comp_name);

		setRobotGoal(c_name, rd, rc, sub);
    }
}
//----------------------------------------------------------------------------------------------------------------------
void Startup::reset()
{
    detail(mName, "reset");
    mForward.reset();
    mLeft.reset();
    mRight.reset();
    ActiveMixIn::resetActive();
}
