//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "ProjectStd.h"
#include "Journal.h"

#include "Behaviors.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
void FindImage::operator()(RobotData& rd, RobotControl& rc, Subsumption& sub) 
{
    detail(mName);
    if (!mPreempted) {
        detail("Image", "X", rd.mPosX, "Y", rd.mPosY, "msecs", rd.mPanPos);

        if (rd.mPosX != 0 && rd.mPosY != 0) {   // found the image location

            if (!isActive()) {      // 'twern't active so get the current pan position
                mRcPos = rd.mPanPos;
            }
            rc.mPan = mRcPos;     // keep pan pointing at base

            setActive();

            mPosX = rd.mPosX;
            mWhere = front;
            mSeeing = home_front;

            mPreempted = true;
        }
    }
}
//----------------------------------------------------------------------------------------------------------------------
void FindImage::reset()
{
    static ImageLocation bl;
    detail("reset");
    
    mPosX = 0;
    mWhere = front;
    mRcPos = 1500;
    mSeeing = nothing;

    mIsPanStopped = false;
    ActiveMixIn::resetActive();
    IndirectMixIn::reset();
}
//----------------------------------------------------------------------------------------------------------------------
void TurnToImage::operator()(RobotData& rd, RobotControl& rc, Subsumption& sub) 
{
    detail(mName);

    if (isActive() || (!mPreempted && mSeeing != nothing)) {  // base found during the pan
        detail(mName, "found");

        setActive();
        image_to_angle(mPosX, mRcPos);
        which_direction();
        detail(mName, "PosX", mPosX, "RcPos", mRcPos, "Angle", mAngle);

        Move& m = dynamic_cast<Move&>( (mWhere == front) ? mForward : (mWhere == left ? mLeft : mRight));

        m(rd, rc, sub);
        if (!mPreempted) {
            resetActive();
            mSeeing = nothing;
        }
    }
}
//----------------------------------------------------------------------------------------------------------------------
void TurnToImage::reset()
{
    detail(mName, "reset");
    mForward.reset();
    mLeft.reset();
    mRight.reset();
    ActiveMixIn::resetActive();
}
