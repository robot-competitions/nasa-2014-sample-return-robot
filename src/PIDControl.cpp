//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "PIDControl.h"
#include "Journal.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
double PIDControl::update(const double error)
{
    double IntegralLimit = 10;
    double p_term, d_term, i_term;

    mErrorSum += error;
    p_term = mProportionalGain * error;   // calculate the proportional term
    i_term = mIntegralGain * mErrorSum;  // calculate the Integral term
    d_term = mDerivativeGain * (error - mLastError);

    // limit the integral gain for next pass...
    // ... provide anti-windup by lowering the integrated error
    if (abs(i_term) > IntegralLimit) {
        if (i_term > 0)
            i_term = IntegralLimit;
        else
            i_term = -IntegralLimit;
        mErrorSum -= error;
    }
    if (error == 0) {
        mErrorSum = 0;
    }

    mLastError = error;
    return p_term + d_term + i_term;
}
//----------------------------------------------------------------------------------------------------------------------
void PIDControl::clearIntegrator()
{
    mErrorSum = 0.0;
}