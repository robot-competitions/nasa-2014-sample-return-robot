//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include <algorithm>
#include <sstream>
#include <ctime>
using namespace std;

#include "ProjectStd.h"
#include "Thread.h"
#include "ThreadTimer.h"
#include "Journal.h"
#include "Configuration.h"

#include "RoboRealm.h"
#include "RRThread.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
RRThread::RRThread(RoboRealm& rr, int& wd) :
    Thread(wd), mRoboRealm(rr) {
    mRoboRealm.init();
}
//--------------------------------------------------------------------------------------------------------------------------
void RRThread::run() {
    trace("RR Thread running");
    ThreadTimer tt(Configuration::mUpdateRate);

    ::sleep(Configuration::mUpdateRate);

    // keep running until the stop event is signaled
    while (isStopping()) {
        clock_t ct = clock();

        read();
        write();

        detail("rr run()", clock() - ct);

        tt.wait();
    }
    trace("RR Thread stopped");
}
//----------------------------------------------------------------------------------------------------------------------
void RRThread::read() {
    detail("collectData");
    int cnt = mRoboRealm.read();
    detail("collectData", cnt);
    //if (cnt == 0) {
    //    throw runtime_error("RRThread: RoboRealm Connection Lost");
    //}
}
//----------------------------------------------------------------------------------------------------------------------
void RRThread::write() {
    detail("write");
    mRoboRealm.write();
    detail("write", "done");
}
