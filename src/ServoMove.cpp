//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "ProjectStd.h"
#include "Journal.h"

#include "Behaviors.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
void ServoMove::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) 
{
    detail(mName);
    if (!mPreempted) {
        
        if (isActive()) {
            detail(mName, "Active");

            if (rd.mIsMoving) {
                detail(mName, "Moving");
                mPreempted = mToPreempt;  // servos are moving so hold off other behaviors until stopped unless mToPreempt false
                if (mOther) {
                    mOther->reset();
                }
            }
        }
        else {
            detail(mName, "Active");
            setActive();
            mPreempted = mToPreempt;  // servos are moving so hold off other behaviors until stopped unless mToPreempt false
        }

        setServos(rc);
        detail(mName, "pan", rc.mPan, "tilt", rc.mTilt, "lift", rc.mLift);
    }
}
//----------------------------------------------------------------------------------------------------------------------
void ServoMove::setServos(RobotControl &rc)
{
    rc.mPan = mPan;
    rc.mTilt = mTilt;
    rc.mLift = mLift;
}
//======================================================================================================================
void ServoPan::operator()(RobotData& rd, RobotControl& rc, Subsumption& mSubsumption) 
{
    detail(mName);
    if (!mPreempted) {

        mPreempted = mToPreempt;

        if (mIsPanActive) {

            if (rd.mIsMoving) {     
                if (mOther) {
                    mOther->reset();
                }
            }
            else {
                if (mIsPanningRight) {  // was moving to the back via the right, now move to back around the front via left
                    mIsPanningRight = false;
                    mPan = PAN_BACK_LEFT;
                }
                else {  // reached the left back position so no results
                    mPreempted = false;
                }
            }
        }
        mIsPanActive = true;
        setServos(rc);

        detail(mName, "pan", rc.mPan, "tilt", rc.mTilt, "lift", rc.mLift);
    } 
}
//----------------------------------------------------------------------------------------------------------------------
void ServoPan::reset()
{
    mPan = PAN_BACK_RIGHT;
    mIsPanningRight = true;
    mIsPanActive = false;
}