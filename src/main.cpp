//======================================================================================================================
// 2013 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "OS.h"
#include "Thread.h"
#include "ProjectStd.h"
#include "Journal.h"

#include "SRR.h"

using namespace std;
//----------------------------------------------------------------------------------------------------------------------
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//---------------------------------------------------------------------------------------------------------------------
void run(SRR& app ) 
{
    try
    {
        app();
    }
    catch (runtime_error& e)
    {
        journal("**EXCEPTION** in SRR(): ", e.what());
    }
}
//----------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[]) 
{
    // Setup the journal and load configuration data 
    JournalEmit j(JournalEmit::eDetail, "srr", "0", true);
    Configuration config;

    try
    {
        SRR app;
        run(app);
    }
    catch (runtime_error& e)
    {
        journal("**EXCEPTION** in APP Constructor: ", e.what());
    }

    return 0;
}
