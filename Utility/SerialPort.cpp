//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//      $Id: $
//
//======================================================================================================================
//

#include "ProjectStd.h"
#include "SerialPort.h"
#include <stdexcept>
#include <string>
using namespace std;

//-----------------------------------------------------------------------------
char port_err[] = "        Bad Serial Port Handle";
//-----------------------------------------------------------------------------
SerialPort::SerialPort(const char* port_name, unsigned int baud)
{
    strncpy(mPortName, port_name, sizeof mPortName);

    mPort  = CreateFile(mPortName,
        GENERIC_READ | GENERIC_WRITE,
        0,
        0,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        0 );

    if (INVALID_HANDLE_VALUE == mPort) {
        memcpy(port_err, mPortName, strlen(mPortName));
        throw runtime_error(port_err);
    }

    // GetCommProperties - get struct to see if can change state values

    DCB dcb = {0};
    if (!GetCommState(mPort, &dcb)) {
        throw runtime_error("Could not get port DCB.");
    }

    dcb.BaudRate = baud;
    dcb.fParity = FALSE;
    dcb.fOutxCtsFlow = FALSE;
    dcb.fOutxDsrFlow = FALSE;
    dcb.fDtrControl = DTR_CONTROL_DISABLE;
    dcb.fDsrSensitivity = FALSE;
    dcb.fOutX = FALSE;
    dcb.fInX = FALSE;
    dcb.fRtsControl = RTS_CONTROL_DISABLE;
    dcb.fAbortOnError = FALSE;
    dcb.unsigned char Size = 8;
    dcb.Parity = NOPARITY;
    dcb.StopBits = ONESTOPBIT;

    if (!SetCommState(mPort, &dcb )) {
        throw runtime_error("Could not set port DCB: " + string(port()));
    }

    COMMTIMEOUTS ct = {0};
    GetCommTimeouts(mPort, &ct);
    ct.ReadIntervalTimeout = 5;
    ct.ReadTotalTimeoutMultiplier = 5; 
    ct.ReadTotalTimeoutConstant = 20; 
    ct.WriteTotalTimeoutConstant = 5;
    ct.WriteTotalTimeoutMultiplier = 5;
    SetCommTimeouts(mPort, &ct);

    SetCommMask(mPort, EV_RXCHAR);
}
//-----------------------------------------------------------------------------
SerialPort::~SerialPort()
{
    if (mPort) {
        CloseHandle(mPort);
    }
    mPort = 0;
}
//-----------------------------------------------------------------------------
void SerialPort::flush()
{
//    ::FlushFileBuffers(mPort);
    ::PurgeComm(mPort, PURGE_RXABORT | PURGE_RXCLEAR | PURGE_TXABORT | PURGE_TXCLEAR);
}
//-----------------------------------------------------------------------------
const unsigned int SerialPort::isRxReady() volatile
{
    COMSTAT stats;
    DWORD err;  // CE_BREAK
    if (ClearCommError(mPort, &err, &stats)) 
    {
        return stats.cbInQue;
    }
    return 0;
}
//-----------------------------------------------------------------------------
DWORD SerialPort::read(unsigned char  buffer[], const unsigned int len) const
{
    DWORD cnt = -1;
    DWORD err;

    for (size_t retries = 0;  retries < 3; retries++) {
        if (ReadFile(mPort, buffer, len, &cnt, NULL)) break;    // successful read
        err = GetLastError();

        COMSTAT stats;
        ClearCommError(mPort, &err, &stats);
    }
    return cnt;
}
//-----------------------------------------------------------------------------
unsigned int SerialPort::write(const unsigned char  buffer[], const unsigned int cnt) const
{
    DWORD c = -1;
    DWORD err;

    for (size_t retries = 0;  retries < 3; retries++) {
        if (WriteFile(mPort, buffer, cnt, &c, NULL)) break;    // successful read
        err = GetLastError();

        COMSTAT stats;
        ClearCommError(mPort, &err, &stats);
    }
    return c;
}
