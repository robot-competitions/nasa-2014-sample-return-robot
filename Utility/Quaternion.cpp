//======================================================================================================================
// 2014 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <iomanip>
using namespace std;

#include "Quaternion.h"
#include <math.h>
//======================================================================================================================
Quaternion::Quaternion(double x, double y, double z) : w(mScalar), x(mVect[0]), y(mVect[1]), z(mVect[2]) 
{
    setQuaternion(x, y, z);
}
//----------------------------------------------------------------------------------------------------------------------
Quaternion::Quaternion(const Vector3d& v) : w(mScalar), x(mVect[0]), y(mVect[1]), z(mVect[2])
{
    setQuaternion(v[0], v[1], v[2]);
}
//----------------------------------------------------------------------------------------------------------------------
Quaternion::Quaternion(const double qw, const double qx, const double qy, const double qz) : w(mScalar), x(mVect[0]), y(mVect[1]), z(mVect[2])
{
    w = qw;
    x = qx;
    y = qy;
    z = qz;
}
//----------------------------------------------------------------------------------------------------------------------
Quaternion::Quaternion(const Quaternion& q) : w(mScalar), x(mVect[0]), y(mVect[1]), z(mVect[2])
{
    w = q.w;
    x = q.x;
    y = q.y;
    z = q.z;
}
//----------------------------------------------------------------------------------------------------------------------
Quaternion::Quaternion(const double& s, const Vector3d& v) : mScalar(s), mVect(v), w(mScalar), x(mVect[0]), y(mVect[1]), z(mVect[2])
{
}
//----------------------------------------------------------------------------------------------------------------------
// Routine used by constructors to set values from angle input as individual values or a Vector3d
// 
void Quaternion::setQuaternion(double x, double y, double z)
{
    double sx = sin(x / 2.0);
    double cx = cos(x / 2.0);

    double sy = sin(y / 2.0);
    double cy = cos(y / 2.0);

    double sz = sin(z / 2.0);
    double cz = cos(z / 2.0);

    mScalar  = cx * cy * cz + sx * sy * sz;
    mVect[0] = sx * cy * cz - cx * sy * sz ;
    mVect[1] = cx * sy * cz + sx * cy * sz;
    mVect[2] = cx * cy * sz - sx * sy * cz;
}
//======================================================================================================================
inline
void Quaternion::justify(double& ax) const
{
    if (ax < 0.0) ax += M_TAU;
    ax = fmod(ax, M_TAU);
}
//----------------------------------------------------------------------------------------------------------------------
Vector3d Quaternion::angles() const {
    const static double EPSILON = 1e-10;

    const double& x = mVect[0];
    const double& y = mVect[1];
    const double& z = mVect[2];
    const double& w = mScalar;

    Vector3d angles;
    double& ax = angles[0];
    double& ay = angles[1];
    double& az = angles[2];

    double sqx = x*x;
    double sqy = y*y;
    double sqz = z*z;
    double sqw = w*w;

    const double test = (x*y + z*w);

    // roll, pitch, yaw

    const double epsilon = 0.500;
    if (test > epsilon) {
        ax = atan2(x, w);
        ay = M_PI_2;
        az = atan2(y, w);
    } else if (test < -epsilon) {
        ax = -atan2(x, w);
        ay = -M_PI_2;
        az = -atan2(y, w);
    }
    else {
        ax = atan2(2*(w*x + y*z),  1.0 - 2*(sqx + sqy));
        ay = asin(2*(w*y - x*z));
        az = atan2(2*(w*z + x*y),  1.0 - 2*(sqy + sqz));

    }
    justify(ax);
    justify(ay);
    justify(az);

    return angles;
}
//----------------------------------------------------------------------------------------------------------------------
//get modulus of a Quaternion sqrt(w^2+x^2+y^2+y^2)
double Quaternion::norm() const {
    const Vector3d& v = this->mVect;
    double R;  
    R =  v[0]*v[0]; 
    R += v[1]*v[1];
    R += v[2]*v[2];
    R += mScalar*mScalar;
    return R;
}
//----------------------------------------------------------------------------------------------------------------------
//convert Q to a Q with same direction and modulus 1
Quaternion& Quaternion::normalize() {
    Vector3d& v = this->mVect;
    double R = magnitude();
    v /= R;
    //v[0] /= R;
    //v[1] /= R; 
    //v[2] /= R;
    mScalar /= R;
    return *this;
}
//----------------------------------------------------------------------------------------------------------------------
void Quaternion::display(ostream& os) const
{
    const Quaternion& q = *this;
    os << setiosflags( ios::fixed ) << setprecision(PRECISION) << showpoint;
    os << setw(WIDTH) << q.mScalar;
    os << setw(WIDTH) << q.mVect;
}
//----------------------------------------------------------------------------------------------------------------------
Quaternion Quaternion::inner(const Quaternion& q) const
{
    const Vector3d& mv = this->mVect;
    const Vector3d& qv = q.mVect;

    return Quaternion(q.mScalar*mScalar, qv[0]*mv[0], qv[1]*mv[1], qv[2]*mv[2]);
}
//----------------------------------------------------------------------------------------------------------------------
double Quaternion::dot(const Quaternion& rhs) const
{
    return mScalar*rhs.mScalar + mVect.dot(rhs.mVect);
}
//----------------------------------------------------------------------------------------------------------------------
bool Quaternion::isIdentical(const Quaternion& rhs) const    
{
    const Quaternion& lhs = *this;
    return dblEq(lhs.dot(rhs), 1.0);
}
//----------------------------------------------------------------------------------------------------------------------
bool Quaternion::isDual(const Quaternion& rhs) const
{
    const Quaternion& lhs = *this;
    return dblEq(pow(lhs.dot(rhs), 2), 1.0);
}
//----------------------------------------------------------------------------------------------------------------------
void Quaternion::operator=(const Quaternion& rhs)
{
    if (this != &rhs) {
        mScalar = rhs.mScalar;
        mVect = rhs.mVect;
    }
}
//----------------------------------------------------------------------------------------------------------------------
const Quaternion& Quaternion::operator*=(const Quaternion& rhs) 
{
    //Quaternion temp(
    //    mScalar * v0.mScalar - mVect.dot(v0.mVect),
    //    (v0.mVect * mScalar) + (mVect * v0.mScalar) + (mVect * v0.mVect)
    //    );

    Quaternion temp(
        w*rhs.w - x*rhs.x - y*rhs.y - z*rhs.z,
        y*rhs.z - z*rhs.y + x*rhs.w + w*rhs.x,
        z*rhs.x - x*rhs.z + y*rhs.w + w*rhs.y,
        x*rhs.y - y*rhs.x + z*rhs.w + w*rhs.z
        );
    *this = temp;
    return *this;
}
