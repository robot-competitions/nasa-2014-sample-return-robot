//=====================================================================================================================
// � 2006 Copyright Mystic Lake Software
// Reproduction, adaptation or translation without prior written permission is prohibited, 	
// except as allowed under the copyright laws.
//=====================================================================================================================
//
//     Author: Rudyard Merriam 
//
//      $Id: Journal.cpp 783 2010-02-03 21:20:32Z rmerriam $
//
//=====================================================================================================================
//
#include "ProjectStd.h"

#include <io.h>

#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <ctime>

#include <sys/timeb.h>

#include "Journal.h"
#include "teebuf.h"

bool JournalEmit::mActive = false;
CRITICAL_SECTION JournalEmit::mCriticalSection;
string JournalEmit::mCurDate("");

string JournalEmit::mFNameBase("");
string JournalEmit::mFName("");

JournalEmit::Level JournalEmit::mLevel;
ofstream* JournalEmit::mLogFile = 0;
ostream* JournalEmit::mOutStream = 0;

bool JournalEmit::mUseCerr;
string JournalEmit::mVersion("0.0.0");

char TAB = '\t';
char* journal_opened = "Journal opened  =====================================";
//-----------------------------------------------------------------------------------------------------------------
static Journal journal(JournalEmit::eLog, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
//=====================================================================================================================
Journal::Journal(JournalEmit::Level level, char* location) : mLocation(location), mLevel(level)
{
    size_t pos = mLocation.rfind("\\")+1;
    mLocation.erase(0, pos);
    mLocation += " ";
    mLocation.erase(mLocation.rfind("."));
}
//----------------------------------------------------------------------------------------------------------------------
void Journal::operator()(const char* msg, char* s1, const long& v1, char* s2, const long& v2) const
{
    stringstream s;

    s << s1 << " " << v1 << "   " << s2 << " " << v2;
    emit(msg, s.str().c_str());
}
//----------------------------------------------------------------------------------------------------------------------
void Journal::operator()(const char* msg, char* s1, const long& v1, char* s2, const long& v2, char* s3, const long& v3) const
{
    stringstream s;

    s << s1 << " " << v1 << "   " << s2 << " " << v2 << "   " << s3 << " " << v3;
    emit(msg, s.str().c_str());
}
//----------------------------------------------------------------------------------------------------------------------
void Journal::operator()(const char* msg, const bool a, const bool b, const bool c, const bool d) const
{
    stringstream s;
    boolalpha(s);

    s << a << TAB << b << TAB << c << TAB << d;
    emit(msg, s.str().c_str());
}
//=====================================================================================================================
JournalEmit::JournalEmit(Level level, const char* fbase, const char* version, bool usecerr)
{
    ::InitializeCriticalSection(&mCriticalSection);

    mActive = true;
    mLevel = level; 
    mVersion = version;
    mFNameBase = fbase;
    mUseCerr = usecerr;

    if (mFNameBase.length() != 0)
    {
        newFile();
    }
    else
    {
        mOutStream = &cerr;
    }
    journal(journal_opened);
}
//-----------------------------------------------------------------------------------------------------------------
JournalEmit::~JournalEmit()
{
    mActive = false;
    journal("Journal closed");
    if (mLogFile)
    {
        mLogFile->close();
        delete mLogFile;
    }
}
//-----------------------------------------------------------------------------------------------------------------
inline
void JournalEmit::cleanup()
{
    journal("Cleanup old journal files");

    long hFile;
    struct _finddata_t j_file;

    time_t tt = time(0);
    tt -= 60 *  // secs
        60 *  // mins
        24 *  // hrs
        30;   // days

    if( (hFile = _findfirst( "*.journal", &j_file )) != -1L )
    {
        do
        {
            if (j_file.time_write < tt)
            {
                trace(j_file.name,  " deleted");
                ::remove(j_file.name);
            }
        } while( _findnext( hFile, &j_file ) == 0 );

        _findclose(hFile);
    }
}
//--------------------------------------------------------------------------------------------
//  Remove chars from a string
inline 
void remove(string& data, char ch)
{
    for (size_t pos = data.find(ch); pos != string::npos; pos = data.find(ch))
    {
        data.erase(pos, 1);
    }
}
//-----------------------------------------------------------------------------------------------------------------
inline void JournalEmit::newFile()
{
    journal("Journal " + mFName + " closed");

    char dbuf[9];
    _strdate_s(dbuf);
    mCurDate = dbuf;

    mFName = mFNameBase;

    mFName += dbuf;
    remove(mFName, '/');

    mFName += ".journal";

    if (mLogFile)
    {
        mLogFile->close();
    }
    delete mLogFile;

    mLogFile = new ofstream(mFName.c_str(), ios_base::out | ios_base::app);
    if (mUseCerr)
    {
        mOutStream = new ostream(new teebuf(cerr.rdbuf(), mLogFile->rdbuf()));
    }
    else
    {
        mOutStream = mLogFile;
    }

    *mOutStream << "\n\n";
    journal("Journal " + mFName + " opened =======================================");
    journal(mFNameBase, mVersion);

    cleanup();
}
//-----------------------------------------------------------------------------------------------------------------
void JournalEmit::emit(const char* location, const char* msg, const char* text, Level level)
{
    if (!mActive) return;

    static unsigned char emit_count = 0;

    EnterCriticalSection(&mCriticalSection);

    emitTime();
    if (mLevel != eLog)
    {
        *mOutStream << setw(2) << ( (level == eTrace) ? "T" : "D");
        *mOutStream << setw(12) << setiosflags(ios::left) << location << " ";
    }
    if (strlen(msg) > 0)
    {
        *mOutStream << msg << ": ";
    }
    *mOutStream << text << endl; 

    if ((mLogFile) && ((emit_count++ % 10) == 0))
    {
        mLogFile->flush();

        char dbuf[9] = {0};
        _strdate_s(dbuf);

        if (string(dbuf) != mCurDate)
        {
            newFile();
        }
    }
    LeaveCriticalSection(&mCriticalSection);
}
//----------------------------------------------------------------------------------------------------------------------
void JournalEmit::setLevel(Level val)
{
    static const char* text[3] = {"eDetail=1", "eTrace", "eLog" };
    mLevel = val;
    journal("Journal level", text[mLevel]);
}
//----------------------------------------------------------------------------------------------------------------------
void JournalEmit::emitTime()
{

    // Get time in seconds and milliseconds
    __timeb32 t_struct;
    _ftime32(&t_struct);

    tm* t = _localtime32(&t_struct.time);

    size_t w = mOutStream->width(2);
    mOutStream->fill('0');

    *mOutStream
        << t->tm_hour << ":"
        << t->tm_min << ":"
        << t->tm_sec << ":"
        << setw(3) << t_struct.millitm;

    mOutStream->fill(' ');
    mOutStream->width(w);

    *mOutStream << " ";
}
