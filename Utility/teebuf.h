//=====================================================================================================================
// � 2006 Copyright Mystic Lake Software
// Reproduction, adaptation or translation without prior written permission is prohibited, 	
// except as allowed under the copyright laws.
//=====================================================================================================================
//
//     Author: Rudyard Merriam 
//
//      $Id: teebuf.h 783 2010-02-03 21:20:32Z rmerriam $
//
//=====================================================================================================================
//
#ifndef teebuf_Include
#define teebuf_Include

//-----------------------------------------------------------------------------------------------------------------
class teebuf: public streambuf 
{
public:
    typedef char_traits<char> traits_type;
    typedef traits_type::int_type  int_type;

    teebuf(streambuf* sb1, streambuf* sb2) : m_sb1(sb1), m_sb2(sb2)
    {}

    int_type overflow(int_type c) 
    {
        if (m_sb1->sputc(c) == traits_type::eof() || 
            m_sb2->sputc(c) == traits_type::eof())
            return traits_type::eof();
        return c;
    }
private:
    streambuf* m_sb1;
    streambuf* m_sb2;
};
#endif