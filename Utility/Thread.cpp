//======================================================================================================================
// 2012 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
#include "ProjectStd.h"
#include "Thread.h"
#include "Journal.h"
//----------------------------------------------------------------------------------------------------------------------
static Journal detail(JournalEmit::eDetail, __FILE__);
static Journal trace(JournalEmit::eTrace, __FILE__);
static Journal journal(JournalEmit::eLog, __FILE__);
//----------------------------------------------------------------------------------------------------------------------
HANDLE Thread::mStopEvent = CreateEvent(0, true, false, "stop event");
volatile int Thread::mActiveCnt = 0;
size_t Thread::mStopDelay;
//----------------------------------------------------------------------------------------------------------------------
Thread::~Thread()
{
    detail("~Thread");
    CloseHandle(mThreadHandle);
}
//---------------------------------------------------------------------------------------------------------------------
void Thread::operator()()
{
    mThreadHandle = CreateThread(NULL, 0, runHelper, this, 0, &mID);
    if (mThreadHandle == NULL) 
    {
        throw ThreadException("Thread create failed.", "");
    }
    mActiveCnt++;
}
//---------------------------------------------------------------------------------------------------------------------
unsigned long WINAPI Thread::runHelper(void* thread)
{
    try {
	    static_cast<Thread*>(thread)->run();
    }
    catch (runtime_error& e)
    {
        journal("**EXCEPTION** in runHelper: ", e.what());
        mActiveCnt--; 
        stop();
    }
    mActiveCnt--; 
    return 0;
}
//---------------------------------------------------------------------------------------------------------------------
void Thread::stop()
{ 
    journal("Setting stop");
    SetEvent(mStopEvent);
    journal("Stop set");
    
    size_t cnt = 20;
    while (mActiveCnt > 0 && cnt-- > 0) {
        detail("Active count", mActiveCnt);
        ::Sleep(mStopDelay);
    }
}
//----------------------------------------------------------------------------------------------------------------------
bool Thread::isStopping() const
{
    mWatchdog = -1;
    return WaitForSingleObject(mStopEvent, 0) !=  WAIT_OBJECT_0;
}
//=====================================================================================================================
ThreadException::ThreadException(const char* msg, const char* desc) : 
runtime_error(msg)
{
    mWhat = runtime_error::what();
    journal(mWhat);
}
//---------------------------------------------------------------------------------------------------------------------
const char* ThreadException::what() const
{
    return mWhat.c_str();
}
